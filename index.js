var fs = require("fs");
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var vhost = require("vhost");
var app = express();
var models = require("./models");
var multer = require("multer")
var async  = require("async")

GLOBAL.app = app;
GLOBAL.path = require("path");


String.prototype.isNumeric = function (obj){
 return ((typeof( obj ) != "Array") && (obj - parseFloat( obj ) + 1) >= 0)
}

var defaultParams = {initPath: __dirname};

var api = require("./apps/api/api")(defaultParams);
var adm = require("./apps/adm/index")(defaultParams);

//app.use(vhost('api.estrelando', api))
//app.use(vhost('adm.estrelando', adm))

app.use('/uploads', express.static('uploads'));
app.use('/images', express.static('images'));
//app.use('/', api);
app.use('/', api);


app.use(function (err, req, res, next){
  var status = 404;
  if(err.status){
    status = err.status;
    delete err.status;
  }
  console.log("REQUEST BLOCKED APP");
  return res.status(status).json({error: err})
})


// SAVE ROUTES
/*
models.Editorial.find(2).then(function(editorial){
  editorial.contentEditorialsPath("news").then(function(count){
    console.log(count);
  })
  */
/*
  console.log("START ROUTE");
  editorial.generateRoutes().then(function (){
    console.log("ROTAS SALVAS");
  }).catch(function (){
    console.log("ERROR ROTA");
  })

})
*/

// SAVE Featured
/*
models.Feature.find(1).then(function (featured){
  featured.writeJSON().then(function(){
    console.log("JSON GRAVADO")
  }).catch(function (){
    console.log("JSON NAO GRAVADO")
  });
})
*/

// Content JSON
/*
models.Content.find(1).then(function (content){
  content.writeSiteContent().then(function (){
    return console.log("JSON SAVED");
  })
})
*/

// Crop Files
/*
models.File.findAll().then(function(files){
  //file.cropFormats()
  async.eachSeries(files, function (item, cb){
    item.cropFormats().then(function (){
      return cb(null, item);
    })
  }, function (){
    console.log("DONE");
  })
  //console.log(file.thumbnails);
})
*/
/*
models.Tag.find({where: {slug: "kim-kardashian"}}).then(function (tag){
  tag.writeJSON();
})

models.Special.find({where: {id: 183}}).then(function (tag){
  tag.writeJSON();
})
*/
models.sequelize.sync({force: false}).then(function (){
  	var server = app.listen(3000, function (){
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app running at http://%s:%s', host, port);

  })
})
