var path = require("path");
var colors = require("colors/safe");

function ColorRoute(app){
  return function (req, res, next){
    console.log(colors.green(req.method)+" / "+colors.green(req.url));
    return next();
  }
}
module.exports = function RouteManager (app, routes){
    var $this = this;
    this.app = app;
    var express = require("express");
    this.router = express.Router();

    
    if(this.app.customParams){
      this.app.customParams(this.router);
    }
    
    if(routes){
      this.routes = routes;
    }


    this.loadRoutes = function (){
      this.routes = require("../routes/routes.js");
      this.processRoutes();
    }
    this.processRoutes = function (){
      for( var identifier in this.routes )
      {
        
          var reg = new RegExp("([A-Z]+) (.+)");
          var matches = reg.exec(identifier)
          if(matches){
            var path = matches[2];
            var route = this.routes[identifier];
            var controller = $this.loadController(route.controller)

            var method = "all";

            switch(matches[1]) {
              case "PUT":
                method = "put";
              break;
              case "PATCH":
                method = "patch";
              break;
              case "DELETE":
                method = "delete";
              break;
              case "GET":
                method = "get";
              break;
              case "POST":
                method = "post";
              break;
            }
            
            if(route.restrict && this.app.policies){
              this.router[method](path, ColorRoute(this.app), this.app.policies(route.controller+":"+route.action), controller[route.action])
            } else {
              this.router[method](path, ColorRoute(this.app), controller[route.action])
            }
            
          }
          
      }
      this.app.use("/", this.router);
    }
    this.loadController = function (controller){
      var cont = require(path.join(this.app.locals.dirPath, "controllers/"+controller+"Controller"))()
      return cont;
    }
    if(!this.routes){
      this.loadRoutes();
    } else {
      this.processRoutes();
    }

}
