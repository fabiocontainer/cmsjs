var fs = require("fs");
var moment = require("moment")
var path = require("path")
var Q = require("q")

module.exports = {
  adjustUploadsPath: function(date, baseDir){
    var baseDir = (baseDir)?baseDir:(path.join(__dirname, "..", "uploads"));
    var cDate = this.adjustDatePath(date);

    return path.join(baseDir, cDate);
  },
  adjustDatePath: function (date){
    var date = (!date)?(new Date()):date;
    var cDate = moment(date).format('YYYY/MM/DD/');
    
    return "" + cDate;
  },
  adjustIdPath: function (id){
    var id = "0000"+id;
    var pathDir = id.slice(-4).slice(0, 2)+path.sep+id.slice(-2).slice(0, 2);
    return pathDir;
  },
  generatePath: function(baseDir, desired_path){
    var arr = desired_path.split(path.sep);
    var nPath = ""
    for(var i in arr){
      try { 
        nPath += arr[i] + path.sep;
        fs.lstatSync(path.join(baseDir, nPath));
      } catch(err){
        try {
          fs.mkdirSync(path.join(baseDir, nPath));
        } catch(err){
          console.log(err);
        }
      }
    }
    return path.join(baseDir, nPath);
  },
  writeJSON: function (obj, filename, cPath){
    var deferred = Q.defer();
    
    var json = JSON.stringify(obj);
    var file = path.join(cPath, filename);
    
    try {
      fs.writeFile(file, json, function (err){
        deferred.resolve(file)
      })
      /*
      fs.open(file, "w+", function (err, fd){
        if(err){return deferred.reject(err); }
        fs.write(fd, json, function (err){
          fs.close(fd, function(){
            deferred.resolve(file)
          })
        });
        
      })
      */
    } catch(e){
      deferred.reject("ERROR Opening File");
    }
    
    
    return deferred.promise;
    
  }
}