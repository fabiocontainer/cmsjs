//var globalTunnel = require("global-tunnel");
//globalTunnel.initialize({host:'172.22.14.18', port: 3128})
var fs = require("fs");
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var vhost = require("vhost");
var app = express();
var models = require("./models");
var multer = require("multer")
var async  = require("async")
var configs = require("./config/startup");


GLOBAL.app = app;
GLOBAL.path = require("path");


String.prototype.isNumeric = function (obj){
 return ((typeof( obj ) != "Array") && (obj - parseFloat( obj ) + 1) >= 0)
}

var defaultParams = {initPath: __dirname};

var api = require("./apps/api/api")(defaultParams);
//var adm = require("./apps/adm/index")(defaultParams);


var activateHealth = true;
for(var i in process.argv){
  if(process.argv[i] == "health:deactive"){
    activateHealth = false;
  }
}
if(activateHealth == true){
  app.use("/health", function (req, res){
    res.status(200).json({"status": "ok"}).end();
  })
}


app.use('/', api)
//app.use(vhost('adm.estrelando.com.br', adm))


app.use(function (err, req, res, next){
  var status = 404;
  if(err.status){
    status = err.status;
    delete err.status;
  }
  console.log("REQUEST BLOCKED APP");
  return res.status(status).json({error: err})
})
/*
function initializeContent(){

	async.auto({
	  createFile: function (cb){
	    console.log("FILE");
	    models.File.create({
	      filename: "1.jpg",
	      fileType: "FILE",
	    }).then(function (res){
	      console.log("FILE CREATED");
	      cb(null, res);
	    }).catch(function (){
	      cb(null, res);
	    })
	  },
		createGroup: function (cb){
			console.log("Creating 'Administradores' group");
			// Create Group
			models.Group.create({name: "Administradores"}).then(function (group){return cb(null, group);})
		},
		createUser: function(cb){
			console.log("Creating 'Fabio' user");
			// Create Users
			models.User.create({
				name: "Fabio",
				email: "fabio@containerdigital.com.br",
				password: "f-15ceagle",
				status: 1
			}).then(function (user){return cb(null, user);})
		},
		createApp: function(cb){
			console.log("Creating 'CMSFurious' App");
			// Create Apps
			models.App.create({name: "CMSFurious", status: 1}).then(function (app){return cb(null, app);})
		},
		createSite: function (cb){
		  models.Editorial.create({
		    name: "Estrelando",
		    title: "Estrelando",
		    keywords: "Celebridades",
		    url: "http://www.estrelando.com.br/",
		    template: "home",
		    mobileTemplate: "home",
		    tabletTemplate: "home"
		  }).then(function (editorial){return cb(null, editorial);})
		},
		createEditorial: ['createSite', function (cb, res){
		  models.Editorial.create({
		    name: "Celebridades",
		    title: "Celebridades",
		    keywords: "Celebridades",
		    url: "celebridades/",
		    template: "lista",
		    mobileTemplate: "lista",
		    tabletTemplate: "lista",
		    ParentId: res.createSite.id,
		    SiteId: res.createSite.id
		  }).then(function (editorial){return cb(null, editorial);})
		}],
		createContent: ['createSite', function (cb, res){
		  models.Content.create({
		    title: "Come Here!",
  			intro: "Lorem Ipsum dolor sit amen",
    		type: "news",
    		summary: "<p>Lorem Ipsum dolor sit amen</p>",
    		fullText: "<p>Lorem Ipsum dolor sit amen</p>",
    		writer: "Fábio",
    		writerEmail: "fabio@containerdigital.com.br",
    		titleSEO: "TitleSEO",
    		keywordsSEO: "Keywords",
    		descriptionSEO: "Lorem",
    		status: 'published',
    		SiteId: res.createSite.id,
    		publishedAt: "2010-01-01 00:00:00"
		  }).then(function (result){
		    console.log("CONTENT CREATED");
		    return cb(null, result);
		  }).catch(function (err){
		    console.log(err);
		  })
		}],
		createFeatured: ['createSite', function (cb, res){
		  console.log("CREATE FEATURED");
		  models.Feature.create({
		    title: "Look what have you done",
  			type: "MD",
  			style: "celebridades",
  			link: "http://www.teste.com/",
  			intro: "Lorem Ipsum",
  			fullText: "Lipsum",
  			startDate: "2015-01-01 01:00:00",
  			endDate: "2016-01-01 01:00:00",
		  }).then(function (result){
		    result.setEditorial(res.createSite).then(function (){
		      console.log("Editorial Setup featured CREATED");
		      return cb(null, result);
		    });
		  })
		}],
		createFeatured2: ['createSite', function (cb, res){
		  console.log("CREATE FEATURED")
		  models.Feature.create({
		    title: "Look what have you done2",
  			type: "MD",
  			link: "http://www.teste.com/",
  			intro: "Lorem Ipsum",
  			style: "estilo",
  			fullText: "Lipsum",
  			startDate: "2015-02-01 01:00:00",
  			endDate: "2016-01-01 01:00:00",
		  }).then(function (result){
		    result.setEditorial(res.createSite).then(function (){
		      console.log("Editorial Setup featured CREATED");
		      return cb(null, result);
		    });
		  })
		}],
		createTag: function (cb){
		  console.log("CREATE TAG");
		  models.Tag.create({
		    title: "Derriere",
		    taxonomy: "TAG"
		  }).then(function (result){console.log("TAG CREATED"); return cb(null, result);})
		},
		createProfile: function (cb){
		  console.log("CREATE PROFILE");
		  models.Tag.create({
		    title: "Kim Kardashian",
		    taxonomy: "ART_PROFILE",
		    profileDate: "1980-10-21 00:00:01",
		    profileType: "CELEBRITIES"
		  }).then(function (result){console.log("PROFILE CREATED"); return cb(null, result);})
		},
		createPermissions: [function (cb, results){
		  console.log("CREATE PERMISSION");

			var modules = ["Contents", "Editorials", "Users", "Group", "Featured", "Files", "Tags", "Special"];
			var actions = ["show", "index", "create", "update", "new", "delete"];

			var permissions = [
			  {module: "Home", action: "index"},
			  {module: "Editorials", action: "template"},
			  {module: "Featured", action: "createBulk"},
		    {module: "Questions", action: "create"},
		    {module: "Questions", action: "index"},
		    {module: "Poll", action: "create"},
		    {module: "Poll", action: "index"},
		    {module: "Answer", action: "create"},
		    {module: "QuizResult", action: "create"},
		    {module: "QuizResult", action: "index"},
		    {module: "Contents", action: "updateJson"},
		    {module: "Gallery", action: "create"},
		    {module: "Editorials", action: "sites"},
		    {module: "Editorials", action: "homes"},
		    {module: "Editorials", action: "css"},
		    {module: "Editorials", action: "featureds"},
		    {module: "Users", action: "me"},
			  ]

			for(var i in modules){
				for(var k in actions) {
					permissions.push({module: modules[i], action: actions[k]})
				}
			}
			models.Permission.bulkCreate(permissions).then(function (){
				models.Permission.findAll().then(function (permissions){
					return cb(null, permissions);
				})

			})
		}],
		relations: ['createGroup', 'createUser', function (cb, results){
			console.log("Init Relations")
			var user = results.createUser;
			var group = results.createGroup;

			user.addGroup(group)
			return cb(null, true)

		}],
		groupPermission: ['createGroup', 'createPermissions', function (cb, results){
		  console.log("Create Group Permissions Relations")
			var permissions = results.createPermissions;
			var group = results.createGroup;

			group.addPermissions(permissions).then(function (result){
			  console.log("RELATIONS CREATED")
				cb(null, result);
			})

		}],
		genToken: ['createUser', 'createApp', function (cb){
		  console.log("CREATE TOKEN");
		  models.Token.create({
		    token: "teste",
		    UserId: 1,
		    AppId: 1
		  }).then(function (token){
		    console.log("TOKEN CREATED")
		    return cb(null, token)
		  }).catch(function(err){
		    console.log("ERR")
		    return cb(err, null);
		  })
		}]
	},  function (err, result){
	  console.log(err);
		console.log("Done Initializing Content")
	})


}

*/
// SAVE ROUTES
/*
models.Editorial.find(2).then(function(editorial){
  editorial.contentEditorialsPath("news").then(function(count){
    console.log(count);
  })
  */
/*
  console.log("START ROUTE");
  editorial.generateRoutes().then(function (){
    console.log("ROTAS SALVAS");
  }).catch(function (){
    console.log("ERROR ROTA");
  })

})
*/

// SAVE Featured
/*
models.Feature.find(1).then(function (featured){
  featured.writeJSON().then(function(){
    console.log("JSON GRAVADO")
  }).catch(function (){
    console.log("JSON NAO GRAVADO")
  });
})
*/

// Content JSON
/*
models.Content.find(1).then(function (content){
  content.writeSiteContent().then(function (){
    return console.log("JSON SAVED");
  })
})
*/

// Crop Files
/*
models.File.findAll().then(function(files){
  //file.cropFormats()
  async.eachSeries(files, function (item, cb){
    item.cropFormats().then(function (){
      return cb(null, item);
    })
  }, function (){
    console.log("DONE");
  })
  //console.log(file.thumbnails);
})
*/
/*
models.Tag.find({where: {slug: "kim-kardashian"}}).then(function (tag){
  tag.writeJSON();
})

models.Special.find({where: {id: 183}}).then(function (tag){
  tag.writeJSON();
})
*/
//models.sequelize.sync().then(function (){

  for(var i=2; i<=process.argv.length; i++){
    if(/(port:)([0-9]+)/i.exec(process.argv[i]))
    {
      configs.startPort = /(port:)([0-9]+)/i.exec(process.argv[i])[2];
    }
  }

	var server = app.listen(configs.startPort, function (){
    var host = server.address().address;
    var port = server.address().port;
    console.log('App running at http://%s:%s', host, port);
    //initializeContent()
  })
//})
