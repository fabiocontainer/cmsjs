var fs = require("fs");
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var vhost = require("vhost");
var app = express();
var models = require("./models");
var multer = require("multer")
var async  = require("async")
var __  = require("underscore")
var moment  = require("moment")
var Q  = require("q")

GLOBAL.app = app;
GLOBAL.path = require("path");


String.prototype.isNumeric = function (obj){
 return ((typeof( obj ) != "Array") && (obj - parseFloat( obj ) + 1) >= 0)
}

var defaultParams = {initPath: __dirname};

var api = require("./apps/api/api")(defaultParams);
var adm = require("./apps/adm/index")(defaultParams);

app.use(vhost('api.estrelando', api))
app.use(vhost('adm.estrelando', adm))


app.use(function (err, req, res, next){
  var status = 404;
  if(err.status){
    status = err.status;
    delete err.status;
  }
  console.log("REQUEST BLOCKED APP");
  return res.status(status).json({error: err})
})


var pullAnalytics = require("ga-analytics");

function getIds(item){
  var defer = Q.defer();
  
  if(/(celebridades|estilo|realities|series|teen)\/(.+)\/(.+)-([0-9]+)/.exec(item[0])){
    var id = /(celebridades|estilo|realities|series|teen)\/(.+)\/(.+)-([0-9]+)/.exec(item[0])[4];
    
    models.Migration.find({where: {OldKey: "CONTENT_"+id}}).then(function (migration){
      if(migration){
        defer.resolve(migration.NewID)
      } else {
        defer.reject(false);
      }
      
    })
    
  } else if(/(nota|foto|ranking|forum|quiz)\/(.+)\/(.+)-([0-9]+)/.exec(item[0])){
    var id = /(celebridades|estilo|realities|series|teen)\/(.+)\/(.+)-([0-9]+)/.exec(item[0])[4];
    return defer.resolve(id);
    
  } else {
    defer.reject(false);
    return false;
    
  }
  return defer.promise;
}
function checkItem(item){
  var defer = Q.defer();
  if(/(celebridades|estilo|realities|series|teen)\/(.+)\/(.+)-([0-9]+)/.exec(item[0])){
    var id = /(celebridades|estilo|realities|series|teen)\/(.+)\/(.+)-([0-9]+)/.exec(item[0])[4]; 
  }
  return defer.promise;
}

//process.env.HTTPS_PROXY = 'http://172.22.14.18:3128/';

pullAnalytics({
  dimensions: "ga:pagepath",
  metrics: "ga:pageviews",
  sort: '-ga:pageviews',
  'start-date': moment().subtract(7, 'days').format("YYYY-mm-dd"),
  'end-date': moment().format("YYYY-mm-dd"),
  'limit': 10, 
  clientId: "27799708581.apps.googleusercontent.com",
  serviceEmail: "27799708581@developer.gserviceaccount.com",
  key: path.join(__dirname, "apps/api/config/google-services-private-key.pem"),
  ids: "ga:9707931"
}, function (err, result){
  if(err) { console.log(err); throw err; }
  var ret = {contents: []};
  
  async.eachLimit(result.rows, 1, function (row, cb){
    if(ret.contents.length > 7){
      //console.log("TRAVOU");
      return cb();
    }
    getIds(row).then(function (id){
      //ret.contents.push({"id": id, "row":row})
      
       var params = {
            where: {
                "id": id, 
            }, include: [{
              model: models.FilesContent, 
              include: models.File
            }, {
              model: models.Tag
            }], 
            limit: 1, 
            order: [['publishedAt', 'DESC']
        ]}

        models.Content.findAll(params).then(function (resultados){
          
          if(resultados.length > 0){
            var itemData = resultados[0];
            //console.log(itemData.toJSONLIMITED());
            
              
              
            ret.contents.push(itemData.toJSONLIMITED());
            console.log(ret.contents.length);
          }
          return cb();
        }).catch(function (){
          return cb();
        })



    }).catch(function (){
      return cb();
    })
    
    
    //checkItem(row);
    //console.log(row);
  }, function (err){
    console.log(err);
    if(!err){
      var destFolder = FuriousUtils.adjustIdPath(1);
      var cPath = FuriousUtils.generatePath(path.join(__dirname, "json"), destFolder)


      FuriousUtils.writeJSON(ret, "EDITORIAL_1_moreread.json", cPath).then(function (){
        console.log("WROTE JSON");
        //deferred.resolve(true);
        //return callback()
      }).catch(function(err){
        //deferred.resolve(true);
        //return callback()
      });
    }
    
    
  })
})