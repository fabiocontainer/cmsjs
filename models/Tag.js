"use strict";
var path = require("path");
var Q = require("q");
var __ = require("underscore");
var FuriousUtils = require(path.join(__dirname, "..", "lib/", "FuriousUtils"));

String.prototype.toSlug = function (){
  var replaces = {
    "ã": "a",
    "á": "a",
    "â": "a",
    "à": "a",
    "ä": "a",
    "ç": "c",
    "õ": "o",
    "ó": "o",
    "ô": "o",
    "ö": "o",
    "ò": "o",
    "ü": "u",
    "ú": "u",
    "ù": "u",
    "û": "u",
    " ": "-",
    "é": "e",
    "ë": "e",
    "è": "e",
    "í": "i",
    "ï": "i",
    "ì": "i",
    "î": "i",
    "[:.!?,]": "",
    "(<([^>]+)>)": ""
  }
  var str = this.toLowerCase().trim();
  for(var i in replaces){
    var reg = new RegExp(i, "g")
    str = str.replace(reg, replaces[i]);
  }
  str = str.replace(/\s/ig,'');
  return str;
}




module.exports = function (sequelize, DataTypes){
  var models = sequelize.models;
  var Tag = sequelize.define("Tag", {
      title: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			slug: {
				type: DataTypes.STRING,
			},
			taxonomy: {
			  type: DataTypes.STRING,
			  defaultValue: "TAG"
			},
			job: {
			  type: DataTypes.STRING
			},
			profileDate: {
			  type: DataTypes.DATE,
			},
			profileDeathDate: {
			  type: DataTypes.DATE,
			},
			profileType: {
			  type: DataTypes.STRING,
			  validate: {
					notEmpty: true,
					isIn: [["TEEN", "CELEBRITIES", "SERIE", "REALITY"]]
				}
			}
    }, {
      hooks: {
        beforeValidate: function(instance, options, fn){

          if(instance.title && !instance.slug){
            instance.slug = instance.title.toSlug();
          }
          return fn(null, instance);
        },
        afterCreate: function(instance, options, fn){
          instance.writeJSON();
          return fn(null, instance);
        },
        afterUpdate: function(instance, options, fn){
          instance.writeJSON();
          return fn(null, instance);
        }
      },
      instanceMethods: {
        writeJSON: function (){
          var obj = this;
          var condition = {order: [['title', 'ASC']], include: [models.File]};
          var filename = "TAGS_TAG.json";
          if(obj.taxonomy != "TAG"){
            condition.where = {taxonomy: obj.taxonomy, profileType: obj.profileType};
            filename = "TAGS_"+obj.taxonomy+"_"+obj.profileType+".json";
          }

          return Tag.findAll(condition).then(function (results){
            var res = [];
            __.forEach(results, function (item, index){
              if(obj.taxonomy == "TAG"){
                res.push(item.toJSONLIMITED())
              } else {
                res.push(item.toJSON())
              }

            })

            var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), "")

            return FuriousUtils.writeJSON(res, filename, cPath).then(function (){
                //deferred.resolve(true);
            }).catch(function(err){
              //deferred.resolve(true);
            })

          })
        },
        toJSON: function (){
          var obj = this.get();
          if(obj.TagsContent){
            delete obj.TagsContent;
          }

          delete obj.deletedAt;
          delete obj.createdAt;
          delete obj.updatedAt;

          return obj;
        },
        toJSONLIMITED: function (){
          var obj = this.get();

          for(var i in obj){
            if(["id", "slug"].indexOf(i) < 0){
              delete obj[i];
            }
          }
          return obj;
        },
        writeContentJSON: function (siteId){
          var deferred = Q.defer();
          var obj = this;

          Tag.find({where: {id: obj.id}, include: [models.File]}).then(function (tag){
              tag.getContents({where: {
                SiteId: siteId,
                status: "published"
              },
              include: [
                {model: models.FilesContent, include: models.File},
                {model: models.Tag, where: {id: tag.id}, attributes: []}
              ],
              limit: 1000,
              order: [['publishedAt', 'DESC']]
              }).then(function (contents){
                var result = tag.toJSON();
                result.contents = [];

                __.forEach(contents, function (content, index){
                  result.contents.push(content.toJSONLIMITED());

                })

                var destFolder = FuriousUtils.adjustIdPath(tag.id);

                var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), destFolder)

                FuriousUtils.writeJSON(result, "TAG_"+siteId+"_"+tag.id+".json", cPath).then(function (){
                    deferred.resolve(true);
                }).catch(function(err){
                  deferred.resolve(true);
                })
              })
              /*
              models.Content.findAll({
                where: {
                  SiteId: siteId,
                  status: "published"
                },
                include: [
                  {model: models.FilesContent, include: models.File},
                  {model: models.Tag, where: {id: tag.id}, attributes: []}
                ],
                limit: 1000,
                order: [['publishedAt', 'DESC']]
            }).then(function (contents){
              var result = tag.toJSON();
              result.contents = [];

              __.forEach(contents, function (content, index){
                result.contents.push(content.toJSONLIMITED());

              })

              var destFolder = FuriousUtils.adjustIdPath(tag.id);

              var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), destFolder)

              FuriousUtils.writeJSON(result, "TAG_"+siteId+"_"+tag.id+".json", cPath).then(function (){
                  deferred.resolve(true);
              }).catch(function(err){
                deferred.resolve(true);
              })

            }).catch(function(err){
              console.log(err);
                deferred.resolve(true);
            })
            */
          }).catch(function(err){
            console.log(err);
              deferred.resolve(true);
          })

          return deferred.promise;
        }
      },
      classMethods: {
        associate: function (models){
          Tag.belongsToMany(models.Content, {through: "TagsContent"})
          Tag.belongsToMany(models.PushDevices, {through: "PushDevicesInterests"})
          Tag.belongsTo(models.File)
          Tag.hasOne(models.Special)
        }
      }
    })
  return Tag
}
