var bcrypt = require("bcrypt")

module.exports = function (sequelize, DataTypes){
  var User = sequelize.define("User", {
    name: {
      type: DataTypes.STRING,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: true,
        isUnique: function(value, next){
          User.find({where: {'email': value}, attributes: ['id']}).done(function (error, user){
            if(error)  { return next(error)};
            if(user) {
              return next('E-mail already in use!');
            }
            next();
          })
        }
      }
    },
    passwordHash: DataTypes.STRING,
    password: {
      type: DataTypes.VIRTUAL,
      set: function(val){
        this.setDataValue('password', val);
        this.setDataValue('password_hash', this.salt + val);
      },
      validate: {
        isLongEnough: function (val){
          if(val.length < 7){
            throw new Error("Password must have 7 characters long")
          }
        }
      } 
    },
    status: {
      type: DataTypes.INTEGER,
      validate: {
        isIn: [[1, 0]]
      }
    }
  }, {
      hooks: {
        afterValidate: function (user, options, fn){
          if(typeof(user.password) != 'undefined'){
            bcrypt.genSalt(10, function (err, salt){
              if(err){ fn(err, null); }
              bcrypt.hash(user.password, salt, function (err, hash){
                user.passwordHash = hash;
                fn(null, user);
              })
            })
          }
        }
      },
      classMethods: {
        associate: function (models){
          User.belongsToMany(models.Group, {through: "UsersGroups"}) 
          User.hasMany(models.Token)
        }
      },
      instanceMethods: {
        toJSON: function(){
          var obj = this.get();
          delete obj.passwordHash;
          return obj;
        }
      }
  });
  
  return User;
}