"use strict";

module.exports = function (sequelize, DataTypes){
  var Pool = sequelize.define("Pool", {
      title: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			fullText: {
				type: DataTypes.TEXT,
			},
			votes: {
			  type: DataTypes.INTEGER,
			  defaultValue: 0
			}	
    }, {
      hooks: {
      },
      classMethods: {
        associate: function (models){ 
          Pool.belongsTo(models.Content)
          Pool.belongsTo(models.File)
        }
      }
    })
  return Pool
}