"use strict";

module.exports = function (sequelize, DataTypes){
  var Group = sequelize.define("Group", {
      name: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
    }, {
      classMethods: {
        associate: function (models){ 
          Group.belongsToMany(models.User, {through: "UsersGroups"}) 
					Group.belongsToMany(models.Permission, {through: "GroupsPermissions"})
        }
      }
    })
  return Group
}