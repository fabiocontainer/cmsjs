"use strict";

module.exports = function (sequelize, DataTypes){
  var Answer = sequelize.define("Answer", {
      title: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			fullText: {
				type: DataTypes.TEXT,
			},
			points: {
			  type: DataTypes.INTEGER,
			}
    }, {
      hooks: {
      },
      classMethods: {
        associate: function (models){ 
          Answer.belongsTo(models.Question)
          Answer.belongsTo(models.File)
        }
      }
    })
  return Answer
}