"use strict";
var bcrypt = require("bcrypt")
var async  = require("async")

module.exports = function (sequelize, DataTypes){
  var App = sequelize.define("App", {
      name: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: true
        }
      },
      secret: DataTypes.STRING,
      clientId: DataTypes.STRING,
      status: {
        type: DataTypes.INTEGER,
        defaultValue: 0,
        validate: {
          isIn: [[1, 0]],
        }
      }
    }, {
			hooks: {
				beforeCreate: function (app, options, fn){
					if(!app.secret || !app.clientId){
						bcrypt.genSalt(10, function(err, salt){
							if(err) { return fn(err, null) }
							
							var date = new Date();
							
							async.auto({
								hash_secret: function(cb){
									bcrypt.hash(app.name+"-FuriousCMS-"+date, salt, function(err, hash){
										if(err) { return cb(err, null)}
										app.secret = hash;
										cb(null, app)
									})
								},
								hash_clientId: function (cb){
									bcrypt.hash(app.name+"-Mavericks-ID-"+date, salt, function(err, hash){
										if(err) { return cb(err, null)}
										app.clientId = hash;
										cb(null, app)
									})
								}
							}, function (err, results){
								return fn(err, app)
							})
						})
					}
				},
				beforeUpdate: function (app, options, fn){
					bcrypt.genSalt(10, function(err, salt){
						if(err) { return fn(err, null) }
						
						var date = new Date();
						
						async.auto({
							hash_secret: function(cb){
								bcrypt.hash(app.name+"-FuriousCMS-"+date, salt, function(err, hash){
									if(err) { return cb(err, null)}
									app.secret = hash;
									cb(null, app)
								})
							}
						}, function (err, results){
							return fn(err, app)
						})
					})
				}
			},
      classMethods: {
        associate: function (models){ 
          App.hasMany(models.Token)
        }
      }
    })
  return App
}