"use strict";

module.exports = function (sequelize, DataTypes){
  var Token = sequelize.define("Token", {
      token: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: true
        }
      },
      validUntil: DataTypes.DATE
    }, {
      hooks: {
        afterValidate: function (token, options, fn){
          token.validUntil = (new Date(Date.now() + ((9*(60*60))*1000)));
					fn(null, token);
        }
      },
      classMethods: {
        associate: function (models){ 
          Token.belongsTo(models.User);
          Token.belongsTo(models.App);
        }
      }
    })
  return Token
}