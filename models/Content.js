"use strict";
var FuriousUtils = require("../lib/FuriousUtils");
var __ = require("underscore");
var Q = require("q");
var fs = require("fs")
var path = require("path")
var moment = require("moment");


String.prototype.toSlug = function (){
  var replaces = {
    "ã": "a",
    "á": "a",
    "â": "a",
    "à": "a",
    "ä": "a",
    "ç": "c",
    "õ": "o",
    "ó": "o",
    "ô": "o",
    "ö": "o",
    "ò": "o",
    "ü": "u",
    "ú": "u",
    "ù": "u",
    "û": "u",
    " ": "-",
    "é": "e",
    "ë": "e",
    "è": "e",
    "ê": "e",
    "í": "i",
    "ï": "i",
    "ì": "i",
    "î": "i",
    "[:.!?,]": "",
    "(<([^>]+)>)": ""
  }
  var str = this.toLowerCase().trim();
  for(var i in replaces){
    var reg = new RegExp(i, "g")
    str = str.replace(reg, replaces[i]);
  }
  str = str.replace(/\s/ig,'');
  return str;
}


module.exports = function (sequelize, DataTypes){
  var models = sequelize.models;
  var Content = sequelize.define("Content", {
      title: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
			intro: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			slug: {
				type: DataTypes.STRING,
			},
  		type: {
  			type: DataTypes.STRING,
  			allowNull: false,
  			validate: {
					notEmpty: true,
					isIn: [["news", "gallery", "quiz", "ranking", "poll", "forum", "page"]]
				}
  		},
  		summary: {
  		  type: DataTypes.TEXT,
  		},
  		fullText: {
  		  type: DataTypes.TEXT,
  		},
  		writer: {
  		  type: DataTypes.STRING,
  		  defaultValue: "Da Redação"
  		},
  		writerEmail: {
  		  type: DataTypes.STRING,
  		  validate: {
  		    isEmail: true
  		  }
  		},
  		titleSEO: {
  		  type: DataTypes.STRING
  		},
  		keywordsSEO: {
  		  type: DataTypes.STRING
  		},
  		descriptionSEO: {
  		  type: DataTypes.STRING
  		},
  		status: {
  		  type: DataTypes.STRING,
  		  defaultValue: "draft",
  		  validate: {
  		    notEmpty: true,
					isIn: [["published", "draft"]]
				}
  		},
  		quizType: {
  		  type: DataTypes.STRING,
  		  validate: {
					isIn: [["correct", "profile", "redcarpet", "poll"]]
				}
  		},
  		showHome: {
  		  type: DataTypes.BOOLEAN,
  		  defaultValue: 0
  		},
      votos1: {
        type: DataTypes.INTEGER,
        defaultValue: 0
      },
      votos2: {
        type: DataTypes.INTEGER,
        defaultValue: 0
      },
      votos3: {
        type: DataTypes.INTEGER,
        defaultValue: 0
      },
      votos4: {
        type: DataTypes.INTEGER,
        defaultValue: 0
      },
      votos5: {
        type: DataTypes.INTEGER,
        defaultValue: 0
      },

  		publishedAt: {
  		  type: DataTypes.DATE
  		}
    }, {
      hooks: {
        beforeCreate: function(instance, options, fn){
          if(instance.title && !instance.slug){
            instance.slug = instance.title.toSlug();
          }
          if(instance.status == "published" && typeof(instance.publishedAt) == "undefined"){
            instance.publishedAt = moment().format("yyyy-MM-dd HH:mm:ss")
          }
          return fn(null, instance);
        },
        afterCreate: function(instance, options, fn){
          //if(instance.status == "published"){
            //instance.writeJSON().then(function (){
              //instance.writeSiteContent()
            //})
          //}
          return fn(null, instance);
        },
        afterUpdate: function(instance, options, fn){
          //if(instance.status == "published"){
            //instance.writeJSON().then(function (){
            //  instance.writeSiteContent()
            //})
          //}
          return fn(null, instance);
        }
      },
      instanceMethods: {
        writeSiteContent: function (){
          var deferred = Q.defer();
          /*
          if(this.status == "published"){
            var type = this.type;
            var siteId = this.SiteId;

            return this.getSite().then(function (site){
              return site.writeEditorialContent(type);
            })
          } else {
            deferred.resolve(true);
          }
          */
          deferred.resolve(true);
          return deferred.promise;
        },
        toJSON: function(){
          var obj = this.get();

          if(this.extras){
            obj.extras = this.extras
          }

          if(obj.FilesContents){
            for(var i in obj.FilesContents){
              var relation = obj.FilesContents[i];
              if(relation.type == "THUMBNAIL"){
                obj.Thumbnail = relation.File;
              }
            }
          }
          return obj;
        },
        toJSONLIMITED: function (){
          var obj = this.get();

          if(obj.FilesContents){
            for(var i in obj.FilesContents){
              if(obj.FilesContents[i]['type'] == "THUMBNAIL"){
                obj.Thumbnail = obj.FilesContents[i]['File'];
              }
            }

            if(!obj.Thumbnail && obj.FilesContents.length > 0){
              obj.Thumbnail = obj.FilesContents[0]["File"];
            }

          }

          if(obj.Tags){
            __.forEach(obj.Tags,  function (tag, index){
              delete tag.createdAt;
              delete tag.updatedAt;
              delete tag.job;
              delete tag.profileDate;
              delete tag.profileDeathDate;
              delete tag.profileType;
              delete tag.TagsContent;
            })
          }

          if(obj.quizType != "redcarpet"){
            delete obj.intro;
          }

          delete obj.FilesContents;
          delete obj.fullText;

          delete obj.titleSEO;
          delete obj.keywordsSEO;
          delete obj.descriptionSEO;
          delete obj.writer;
          delete obj.writerEmail;

          delete obj.votos1;
          delete obj.votos2;
          delete obj.votos3;
          delete obj.votos4;
          delete obj.votos5;
          delete obj.deletedAt;


          return obj;
        },
        getExtras: function (){
            var $this = this;
            return sequelize.query("SELECT `Contents`.`title`, `Contents`.`id` FROM `ContentExtras` LEFT JOIN `Contents` ON (`Contents`.`id` = `ContentExtras`.`ParentId`) WHERE `Contents`.`status` = 'published' AND `ContentExtras`.`ChildrenId` = "+this.id, {type: sequelize.QueryTypes.SELECT}).then(function(extras){
              $this.extras = extras;
            })
        },
        writeJSON: function (){
          var deferred = Q.defer();
          /*
          if(this.status == "published"){
            var models = sequelize.models;
            var includes = [
              {model: models.FilesContent, include: models.File, order: [['order', 'ASC']]},
              {model: models.Tag},
              {model: models.Editorial},
            ];

            if(this.type == "quiz"){
              includes.push({model: models.Question, include: models.Answer});
              includes.push({model: models.QuizResult});
            }
            if(this.type == "poll"){
              includes.push({model: models.Pool, include: models.File});
            }
            return Content.find({where: {id: this.id}, include: includes}).then(function (content){
              return content.getExtras().then(function(){
                var obj = content.toJSON();


                var destFolder = FuriousUtils.adjustIdPath(content.id);
                var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), destFolder)

                return FuriousUtils.writeJSON(obj, "CNT_"+content.id+".json", cPath)
              })


            })
          } else {
            deferred.reject(null);
          }
          */
          deferred.resolve(true);
        return deferred.promise;
        }
      },
      getterMethods: {
        url: function (){
          return (this.type && this.publishedAt && this.slug)?"/"+this.type+""+moment(this.publishedAt).format("/YYYY/MM/DD/")+""+this.slug+"-"+this.id:"";
        }
      },
      classMethods: {
        associate: function (models){
          Content.belongsToMany(models.Tag, {through: "TagsContent"})
          Content.hasMany(models.FilesContent)
          Content.hasMany(models.Pool)
          Content.belongsToMany(models.Editorial, {through: "EditorialsContent"})
          Content.belongsTo(models.Editorial, {as: "Site"})
          Content.hasMany(models.Question)
          Content.hasMany(models.QuizResult)
          Content.belongsToMany(Content, {as: "Parent", through: 'ContentExtras', foreignKey: 'ParentId'})
          Content.belongsToMany(Content, {as: "Children", through: 'ContentExtras', foreignKey: 'ChildrenId', constraints: false})
        }
      }
    })
  return Content
}
