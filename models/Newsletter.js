"use strict";

module.exports = function (sequelize, DataTypes){
  var models = sequelize.models;
  var Newsletter = sequelize.define("Newsletter", {
      name: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			email: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true,
					isEmail: true
				}
			}
    }, {
      classMethods: {
        associate: function (models){ 
          Newsletter.belongsTo(models.Editorial, {as: "Site"})
        }
      }
    })
  return Newsletter
}