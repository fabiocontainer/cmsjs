"use strict";

var path = require("path");
var Q = require("q");
var __ = require("underscore");
var FuriousUtils = require(path.join(__dirname, "..", "lib/", "FuriousUtils"));


module.exports = function (sequelize, DataTypes){
  var models = sequelize.models;
  var Special = sequelize.define("Special", {
      title: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
			url: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
			color: {
				type: DataTypes.STRING,
			},
			titleSEO: {
  		  type: DataTypes.STRING
  		},
  		keywordsSEO: {
  		  type: DataTypes.STRING
  		},
  		descriptionSEO: {
  		  type: DataTypes.STRING
  		}
    }, {
      hooks: {
      },
      instanceMethods: {
        writeJSON: function(){
          return Special.findAll({include: [models.File]}).then(function (itens){

            var result = [];

            __.forEach(itens, function (special, index){
              result.push(special.toJSON());
            })

            //var destFolder = FuriousUtils.adjustIdPath(obj.id);

            var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), "")

            return FuriousUtils.writeJSON(result, "SPECIALS.json", cPath)



          })
        }
      },
      classMethods: {
        writeJson: function(){
          return Special.findAll({include: [models.File]}).then(function (itens){

            var result = [];

            __.forEach(itens, function (special, index){
              result.push(special.toJSON());
            })

            //var destFolder = FuriousUtils.adjustIdPath(obj.id);

            var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), "")

            return FuriousUtils.writeJSON(result, "SPECIALS.json", cPath)

          })
        },
        associate: function (models){
          Special.belongsTo(models.Tag)
          Special.belongsTo(models.File)
          Special.belongsTo(models.Editorial, {as: "Site"})
        }
      }
    })
  return Special
}
