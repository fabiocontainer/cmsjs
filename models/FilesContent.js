"use strict";
var Sizes = ["300x300", '100x100', '162x115', '600x360', '450x270', '465x220', '296x180', '230x450', '230x515', '230x220', '539x310', '194x176', '732x398', '335x220', '335x280', '540x515','620x100', '675x645', 'instagram', 'gallery', 'fullgallery'];
var Sizes2 = ["original", "300x300", '100x100', '162x115', '600x360', '450x270', '465x220', '296x180', '230x450', '230x515', '230x220', '539x310', '194x176', '732x398', '335x220', '335x280', '540x515','620x100', '675x645', 'instagram', 'gallery', 'fullgallery'];

module.exports = function (sequelize, DataTypes){
  var FilesContent = sequelize.define("FilesContent", {
      type: {
				type: DataTypes.STRING,
				validate: {
				  notEmpty: true
				}
			},
      title: {
				type: DataTypes.STRING,
			},
			description: {
				type: DataTypes.TEXT,
			},
			credits: {
				type: DataTypes.TEXT,
			},
      fileSize: {
			  type: DataTypes.STRING,
			  validate: {
			    isIn: [Sizes2]
			  }
			},
			order: {
				type: DataTypes.INTEGER,
			}
    }, {
      hooks: {
        afterValidate: function(instance, options, fn){
          if(!instance.fileSize){
            instance.fileSize = (instance.type == "GALLERY")?"gallery":"";
          }
          return fn(null, instance);
        },
      },
      classMethods: {
        associate: function (models){
          FilesContent.belongsTo(models.File)
          FilesContent.belongsTo(models.Content)
        }
      }
    })
  return FilesContent
}
