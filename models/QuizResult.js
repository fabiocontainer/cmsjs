"use strict";

module.exports = function (sequelize, DataTypes){
  var QuizResult = sequelize.define("QuizResult", {
      title: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			fullText: {
				type: DataTypes.TEXT,
			},
			
    }, {
      hooks: {
      },
      classMethods: {
        associate: function (models){ 
          QuizResult.belongsTo(models.Content)
          QuizResult.belongsTo(models.File)
        }
      }
    })
  return QuizResult
}