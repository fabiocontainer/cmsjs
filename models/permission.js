"use strict";

module.exports = function (sequelize, DataTypes){
  var Permission = sequelize.define("Permission", {
      module: {
				type:DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
			action: {
				type:DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
    }, {
      getterMethods: {
          role: function()  { 
            return this.module + ':' + this.action 
          }
      },
      classMethods: {
        associate: function (models){ 
          Permission.belongsToMany(models.Group, {through: "GroupsPermissions"})
        }
      }
    })
  return Permission
}