"use strict";
var path = require("path");
var Q = require("q");
var __ = require("underscore");
var FuriousUtils = require(path.join(__dirname, "..", "lib/", "FuriousUtils"));

module.exports = function (sequelize, DataTypes){
  var Migration = sequelize.define("Migration", {
      OldKey: {
        type: DataTypes.STRING,
      },
      OldID: {
        type: DataTypes.INTEGER,
      },
      NewID: DataTypes.INTEGER
    }, {
      instanceMethods: {
        toJSON: function (){
          var obj = this.get();
          delete obj.id;
          delete obj.OldKey;
          delete obj.createdAt;
          delete obj.updatedAt;
          delete obj.deletedAt;
          return obj;
        }
      },
      classMethods: {
        hooks: {
          beforeCreate: function (app, options, fn){
            var matches = /([0-9]+)/i.exec(app.OldKey);
            if(matches[1]){
              app.OldID = matches[1];
            }
            return fn(null, app);
          }
        },
        associate: function (models){
          //App.hasMany(models.Token)
        },
        writeJSON: function (){
            var deferred = Q.defer();
            
            Migration.findAll({where: {OldKey: {$like: "CONTENT_%"}}, order: [["id", "DESC"]], raw: true}).then(function (contents){
              var ret = {};
              __.forEach(contents, function (content, index){
                ret[content.OldID] = content.NewID;
              })
              
              return FuriousUtils.writeJSON(ret, "migrations2.json", path.join(__dirname, "..", "json")).then(function(){
                deferred.resolve(true);
              }).catch(function (){
                deferred.resolve(true);
              })
              
            }).then(function (){
              deferred.resolve(true);
            }).catch(function (){
              deferred.resolve(true);
            })
            return deferred.promise;
        }
      },
      
    })
  return Migration
}
