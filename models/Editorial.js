var path = require("path");
var util = require("util");

var Q = require("q");
var async = require("async");
var fs = require("fs");
var __ = require("underscore");
var FuriousUtils = require(path.join(__dirname, "..", "lib", "FuriousUtils"));


function getEditorialPath(editorial, cb){
  var path = editorial.url;
  if(editorial.isSite){
    return cb("/");
  } else {
    editorial.getDirectParent().then(function (parent){
      if(!parent) { return cb(path) }
      getEditorialPath(parent, function (parentPath){
        var nPath = parentPath  + "" + path;
        return cb(nPath);
      })
    }).catch(function (){
      return cb(path);
    })
  }
}

module.exports = function (sequelize, DataTypes){
  var models = sequelize.models;
  var Editorial = sequelize.define("Editorial", {
      name: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			title: {
				type: DataTypes.STRING
			},
			keywords: {
				type: DataTypes.STRING
			},
			description: {
				type: DataTypes.TEXT
			},
			template: {
				type: DataTypes.STRING
			},
			mobileTemplate: {
				type: DataTypes.STRING
			},
			tabletTemplate: {
				type: DataTypes.STRING
			},
			url: {
				type: DataTypes.STRING
			},
			routeLevel: {
				type: DataTypes.INTEGER,
				defaultValue: 1
			}
    }, {
      hooks: {
        afterUpdate: function(instance, options, fn) {
          instance.generateRoutes();
          return fn(null, instance);
        },
        afterCreate: function(instance, options, fn) {
          instance.generateRoutes();
          return fn(null, instance);
        }
      },
      getterMethods: {
        isSite: function (){ return (!this.ParentId)?true:false; }
      },
      instanceMethods: {
        writeContentJson: function (){
          var deferred = Q.defer();
          deferred.resolve(true);
          return deferred.promise;
          /*
          var siteId = this.SiteId;
          var editorial = this;

          return this.getChildrenIds().then(function (ids){
            return models.Content.findAll({
              attributes: ["title", "summary", "publishedAt", "type", "slug", "createAt", "intro", "updatedAt"],
              where: {
                SiteId: siteId,
                status: "published",
                type: { $ne: "poll"}
              },
                include: [{
                  model: models.FilesContent,
                  include: models.File
                },
                {
                  model: models.Tag,
                  attributes: ["title", "slug"]
                },
                 {
                  model: models.Editorial,
                  where: {
                    id: {$in: ids}
                }, attributes: []}], limit: 500, order: [['publishedAt', 'DESC']]}).then(function (resultados){

              var results = [];
              __.forEach(resultados, function (content, index){
                results.push(content.toJSON());
              })
              var destFolder = FuriousUtils.adjustIdPath(editorial.id);

              var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), destFolder)
              console.log("Wrote: "+"EDITORIAL_"+siteId+"_"+editorial.id+".json");

              return FuriousUtils.writeJSON(results, "EDITORIAL_"+siteId+"_"+editorial.id+".json", cPath)
            })
          })
          */
        },
        getDirectParent: function (){
          var obj = this;
          var deferred = Q.defer();
          /*
          Editorial.find({where: {id: this.ParentId}}).then(function(parent){
            obj.parent = parent;
            deferred.resolve(obj)
          }).catch(function(err){
            return deferred.reject(err)
          })
          */
          deferred.resolve(true);
          return deferred.promise;
        },
        getCountChildrens: function(){
          return Editorial.count({where: {ParentId: this.id}})
        },
        getChildrens: function(){
          var deferred = Q.defer();
          var obj = this;
          obj.children = [];
          obj.contentQuery = [this.id];
          Editorial.findAll({where: {ParentId: this.id}}).then(function (childrens){
              if(childrens.length < 1) { return deferred.resolve(obj); }

              var promises = [];

              __.forEach(childrens, function (item, index){
                promises.push(item.getChildrens());
              })
              var p = Q.all(promises)

              p.then(function(itens){
                __.forEach(itens, function (item, index){
                  obj.contentQuery = __.union(obj.contentQuery, item.contentQuery);
                })
              }).done(function (){
                deferred.resolve(obj);
              })

          })
          return deferred.promise;
        },
        getChildrenIds: function(){
          var deferred = Q.defer();
          var obj = this;
          if(!this.contentQuery){
            this.getChildrens().then(function (){
              deferred.resolve(obj.contentQuery);
            })
          } else {
            deferred.resolve(obj.contentQuery);
          }

          return deferred.promise;
        },
        generateRoute: function (){
          var deferred = Q.defer();
          getEditorialPath(this, function (path){
            //console.log(path);
            deferred.resolve(path);
          })
          return deferred.promise;
        },
        writeEditorialContentVids: function (){
          var deferred = Q.defer();
          var siteId = (this.SiteId)?this.SiteId:this.id;
          var editorial = this;
          deferred.resolve(true);
          return deferred.promise;
          /*
          models.Editorial.findAll({where: {ParentId: siteId}, raw: true}).then(function (childrenEdts){
            async.eachLimit(childrenEdts, 1, function (edt, callback){

              var sql = "SELECT * FROM ( SELECT `Contents`.*, `EditorialsContent`.`EditorialId` FROM `Contents` LEFT JOIN `EditorialsContent` ON (`EditorialsContent`.`ContentId` = `Contents`.`id`) WHERE `Contents`.`deletedAt` IS NULL AND `EditorialsContent`.`EditorialId` = 7 ) as Contents LEFT JOIN `EditorialsContent` ON (`EditorialsContent`.`ContentId` = `Contents`.`id`)"

              if(edt.id != 7){
                sql += "WHERE `EditorialsContent`.`EditorialId` = 2";
              }

              sequelize.query(sql).spread(function (results, metadata) {

                //console.log(util.inspect(process.memoryUsage().rss));
                //console.log(results);
                var ids = [];
                __.forEach(results, function (item){
                  ids.push(item.id);
                })
                if(ids.length < 1){
                  return callback();
                }

                var params = {
                    where: {
                        //SiteId: siteId,
                        id: {$in: ids}
                    },
                    include: [{
                      model: models.FilesContent,
                      include: [{model: models.File}]
                    }, {
                      model: models.Tag
                    }],
                    raw: true,
                    order: [['publishedAt', 'DESC']]
                  }


                models.Content.findAll(params).then(function (resultados){
                  // Content
                  var ret = {};
                  ret.content = [];

                  var fileName = "EDITORIAL_"+edt.id+"_videos.json";

                  __.forEach(resultados, function (content, index){
                    ret.content.push(content.toJSONLIMITED());
                  })

                  var destFolder = FuriousUtils.adjustIdPath(edt.id);

                  var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), destFolder)
                  console.log("Wrote: "+fileName);


                  FuriousUtils.writeJSON(ret, fileName, cPath).then(function (){
                    delete ret;
                    //global.gc();
                    return callback()
                  }).catch(function(err){
                    delete ret;
                    return callback()
                  });

                  // /Content
                }).catch(function (err){
                  return callback();
                })




                //return callback()
              })


            }, function (){
              //console.log(new Date())
              deferred.resolve(true);
            })
          })

          return deferred.promise;
          */
        },
        writeContentTypeJson: function (type){
          if(this.id == 7){
            //console.log("RODOU VIDEO");
            return this.writeEditorialContentVids();
          }


          var deferred = Q.defer();
          var editorial = this.id;
          var ids = [editorial];
          var cType = type;

          deferred.resolve(true);
          return deferred.promise;
          /*
          var params = {
              where: {
                  status: "published",
                  'type': cType
              }, include: [{
                model: models.FilesContent,
                include: models.File
              }, {
                model: models.Tag
              }, {
                model: models.Editorial,
                where: {id: {$in: ids}},
                attributes: []
              }], limit: 500,
              order: [['publishedAt', 'DESC']
          ]}
          if(cType == "poll"){
            params.include.push({model: models.Pool, include: models.File})
          }
          models.Content.findAll(params).then(function (resultados){
            // Content
            var ret = {};

            ret.content = [];

            var fileName = "EDITORIAL_"+editorial+"_"+cType+".json";


            __.forEach(resultados, function (content, index){
              ret.content.push(content.toJSONLIMITED());
            })
            var destFolder = FuriousUtils.adjustIdPath(editorial);

            var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), destFolder)

            FuriousUtils.writeJSON(ret, fileName, cPath).then(function (){
              console.log("WROTE JSON");
              deferred.resolve(true);
              //return callback()
            }).catch(function(err){
              deferred.resolve(true);
              //return callback()
            });

            // /Content
          }).catch(function (err){
            deferred.resolve(true);
          })
           return deferred.promise;
           */
        },
        writeEditorialContent: function (onlyPools){
          if(this.id == 7){
            return this.writeEditorialContentVids();
          }
          var deferred = Q.defer();
          var siteId = (this.SiteId)?this.SiteId:this.id;
          var editorial = this;
          deferred.resolve(true);

          return deferred.promise;
          /*
          var fileName = "EDITORIAL_"+siteId+"_"+editorial.id+".json";

          editorial.getChildrenIds().then(function (ids){

            var params = {where: {SiteId: siteId, status: "published"}, include: [{model: models.FilesContent, include: models.File}, {model: models.Tag}, {model: models.Editorial, where: {id: {$in: ids}}, attributes: []}], limit: 500, order: [['publishedAt', 'DESC']]}

            if(onlyPools){
              params.where.type = "poll";
              params.where.quizType = onlyPools;

              fileName = "EDITORIAL_"+editorial.id+"_"+onlyPools+".json";
            } else {
              params.where.type = {$ne: "poll"};
            }

            models.Content.findAll(params).then(function (resultados){

              var ret = editorial.toJSON();
              ret.content = [];

              __.forEach(resultados, function (content, index){
                ret.content.push(content.toJSONLIMITED());
              })
              var destFolder = FuriousUtils.adjustIdPath(editorial.id);

              var cPath = FuriousUtils.generatePath(path.join(__dirname, "..", "json"), destFolder)
              console.log("Wrote: "+fileName);

              FuriousUtils.writeJSON(ret, fileName, cPath).then(function (){
                deferred.resolve(editorial);
              }).catch(function(err){
                deferred.reject(editorial);
              });

            }).catch(function(err){
                deferred.reject(err);
            })
          }).catch(function(err){
              deferred.reject(err);
          })
          return deferred.promise;
          */
        },
        contentEditorialsPath: function (poolsConf){
          var obj = this;
          var deferred = Q.defer();
          deferred.resolve(true);
          return deferred.promise;

          /*
          obj.writeEditorialContent(poolsConf).then(function (){
            console.log("Editorails content Generated");

            obj.getDirectParent().then(function (){
              if(!obj.parent){ return deferred.resolve(true); }
              return obj.parent.contentEditorialsPath().then(function(){
                return deferred.resolve(true);
              })
            }).catch(function (err){
              return deferred.resolve(true);
            })
          }).catch(function (err){
            return deferred.resolve(true);
          })
          return deferred.promise;
          */
        },
        generateRouteObject: function(){
          var deferred = Q.defer();
          var item = {
            "template": this.template,
            "mobileTemplate": this.mobileTemplate,
            "tabletTemplate": this.tabletTemplate,
          }
          this.generateRoute().then(function(path){
            item.path = path
          }).done(function (){
            deferred.resolve(item);
          })
          return deferred.promise;
        },
        generateRoutes: function (){
          var deferred = Q.defer();
          var siteID = (this.isSite)?this.id:this.SiteId;
          deferred.resolve(true);
          return deferred.promise;
          /*
          Editorial.findAll({where: {SiteId: siteID, title: {$ne: null}}, order: 'routeLevel DESC'}).then(function (editorials){
            var obj = {};
            async.eachSeries(editorials, function (item, cb){
              item.generateRouteObject().then(function (itemRoute){
                if(itemRoute.path) {
                  obj[itemRoute.path] = itemRoute;
                }

                return cb()
              }).catch(function (){
                return cb()
              })
            }, function (err){


              var json = JSON.stringify(obj);
              try {
                fs.open(path.join(__dirname, "../", "json/", "ROUTES_"+siteID+".json"), "w+", function (err, fd){
                  if(err){return deferred.reject(true); }
                  fs.write(fd, json, function (err){
                  });
                  fs.close(fd, function(){
                    deferred.resolve(true)
                  })
                })
              } catch (e){
                return deferred.reject("Error opening file");
              }
            })


          }).catch(function (err){
            deferred.reject(err)
          })
          return deferred.promise;
          */
        }
      },
      classMethods: {
        associate: function (models){
          Editorial.hasOne(Editorial, {as: 'Parent', foreignKey: 'ParentId'})
          Editorial.hasOne(Editorial, {as: 'Site', foreignKey: 'SiteId'})
          Editorial.hasOne(models.Special, {as: 'Site', foreignKey: 'SiteId'})
          Editorial.belongsToMany(models.Content, {through: "EditorialsContent"})
          Editorial.hasMany(models.Feature)
        }
      }
    })
  return Editorial
}
