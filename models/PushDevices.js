"use strict";

module.exports = function (sequelize, DataTypes){
  var models = sequelize.models;
  var PushDevices = sequelize.define("PushDevices", {
      token: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			type: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
      lastSend: {
				type: DataTypes.DATE,
				defaultValue: sequelize.NOW
			}
    }, {
      classMethods: {
        associate: function (models){
          PushDevices.belongsToMany(models.Tag, {through: "PushDevicesInterests"})
        }
      }
    })
  return PushDevices
}
