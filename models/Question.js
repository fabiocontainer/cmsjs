"use strict";

module.exports = function (sequelize, DataTypes){
  var Question = sequelize.define("Question", {
      title: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			fullText: {
				type: DataTypes.TEXT,
			},
			
    }, {
      hooks: {
      },
      classMethods: {
        associate: function (models){ 
          Question.belongsTo(models.Content)
          Question.hasMany(models.Answer)
          Question.belongsTo(models.File)
        }
      }
    })
  return Question
}