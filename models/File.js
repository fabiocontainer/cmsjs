"use strict";
var __ = require("underscore");
var moment = require("moment");
var path = require("path");
var FuriousUtils = require(path.resolve(__dirname, "..", "lib", "FuriousUtils"));
var gm = require("gm");
var fs = require("fs");
var Q = require("q");
var Sizes = ["300x300", '100x100', '162x115', '600x360', '450x270', '465x220', '296x180', '230x450', '230x515', '230x220', '539x310', '194x176', '732x398', '335x220', '335x280', '540x515','620x100', '675x645', 'instagram', 'gallery', 'fullgallery'];

module.exports = function (sequelize, DataTypes){
  var File = sequelize.define("File", {
      filename: {
				type: DataTypes.STRING,
				allowNull: false,
				validate: {
					notEmpty: true
				}
			},
			fileType: {
			  type: DataTypes.STRING,
			  allowNull: false,
			  validate: {
			    notEmpty: true,
			    isIn: [["FILE", "URL"]]
			  }
			},
			extension: {
				type: DataTypes.STRING,
			},

    }, {
      hooks: {
        afterValidate: function(instance, options, fn){
          if(instance.filename && instance.fileType != "URL"){
            instance.extension = (path.extname(instance.filename)).toLowerCase().replace(".", "");
            if(instance.extension == "jpeg"){
              instance.extension = "jpg";
            }
          }
          return fn(null, instance);
        },
        afterCreate: function (instance, options, fn){
          if(instance.fileType != "FILE"){ return fn(null, instance); }
          if(["jpg", "png", "gif"].indexOf(instance.extension) >= 0){

            instance.cropFormats().then(function (){
              return fn(null, instance);
            }).catch(function (e){
            return fn(null, instance);
            })
          } else {
            return fn(null, instance);
          }
        }
      },
      getterMethods: {
        thumbnails: function (){
          var thumbs = {};
          if(this.isImage){
            var f = this.formats;
            for(var i in f){
              thumbs[(f[i])] = this.formatUrl((f[i]))
            }
          }
          return thumbs;
        },
        formats: function (){
          return Sizes;
        },
        isImage: function (){
          return (["jpg", "png", "gif"].indexOf(this.extension) < 0 || this.fileType != "FILE")?false:true;
        },
        url: function (){
          if(this.fileType == "FILE"){
            var cDate = moment(this.createdAt).format("YYYY/MM/DD/");
            return "/uploads/"+cDate+this.filename
          } else {
            return this.filename;
          }
        }
      },
      instanceMethods: {
          formatUrl: function (format){
            var ext = path.extname(this.filename);
            var cDate = moment(this.createdAt).format("YYYY/MM/DD/");
            var newName = this.filename.replace(ext, "") + "." + format + ext;

            return "/uploads/"+cDate+newName;
          },
          cropFormats: function (){
            var obj = this;
            var formats = this.formats;
            var cropPlay = [];


            //console.log(formats);
            __.forEach(formats, function(format, index){
                cropPlay.push(obj.cropImage(format))
            })

            var deferred = Q.all(cropPlay)

            return deferred
          },
          cropImage: function (format, registerPoint){
            var deferred = Q.defer();
            var registerPoint = (!registerPoint)?"North":registerPoint;
            var filePath = (FuriousUtils.adjustUploadsPath(this.createdAt));
            var ext = path.extname(this.filename);
            var newName = this.filename.replace(ext, "") + "." + format + ext;
            var obj = this;
            var cropMethod = "crop"
            if (format == "gallery" || format == "fullgallery" || format == "instagram") {
              var w = (format == "gallery")?800:1920;
              var h = (format == "gallery")?800:1030;

              w = (format == "instagram")?600:w;
              h = (format == "instagram")?600:h;

              cropMethod = "resize"
            } else {
              var split = format.split("x");
              var w = split[0];
              var h = split[1];
            }
            var nPath = path.join(filePath, newName);

            try {
              fs.readFile(path.join(filePath, obj.filename), function (err, buf){
                //console.log(err);

                if(err){  return deferred.resolve(null); }

                if (cropMethod == "resize") {
                  gm(buf, obj.filename).noProfile().resize(w, h, ">").quality(60).write(nPath, function (err){
                    if(err) { console.log(err); deferred.resolve("null"); }
                    return deferred.resolve({'format': format, 'thumbPath': nPath});
                  })
                } else {
                  gm(buf, obj.filename).noProfile().gravity(registerPoint).thumb(w, h, nPath, 80, function (err){
                    if(err) { console.log(err); deferred.resolve("null"); }
                    return deferred.resolve({'format': format, 'thumbPath': nPath});
                  })
                }

              });
            } catch(e){
              deferred.reject("file error");
            }
            return deferred.promise;
          }
      },
      classMethods: {
        associate: function (models){
          File.hasMany(models.FilesContent)
          File.belongsToMany(models.Feature, {through: "FeaturedFile"})
          File.hasOne(models.Tag)
          File.hasOne(models.Special)
          File.hasOne(models.Pool)
        }
      }
    })
  return File
}
