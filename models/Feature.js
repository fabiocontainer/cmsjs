"use strict";
var FuriousUtils = require("../lib/FuriousUtils");
var __ = require("underscore");
var Q = require("q");
var fs = require("fs")
var path = require("path")

module.exports = function (sequelize, DataTypes){
  var models = sequelize.models;
  var Feature = sequelize.define("Feature", {
      title: {
				type: DataTypes.STRING,
				validate: {
					notEmpty: true
				}
			},
			type: {
			  type: DataTypes.STRING,
			  validate: {
			    notEmpty: true
			  }
			},
			link: {
				type: DataTypes.STRING,
			},
			style: {
			  type: DataTypes.STRING,
			  validate: {
			    notEmpty: true,
					isIn: [["celebridades", "teen", "estilo", "series", "realities", "videos"]]
			  }
			},
			intro: {
				type: DataTypes.STRING,
			},
			fullText: {
				type: DataTypes.TEXT,
			},
			startDate: {
			  type: DataTypes.DATE,
			  validate: {
			    notEmpty: true
			  }
			},
			endDate: {
			  type: DataTypes.DATE,
			  validate: {
			    notEmpty: true
			  }
			}
    }, {
      hooks: {
        afterUpdate: function(instance, options, fn) {
          //instance.writeJSON();
          return fn(null, instance);
        },
        afterCreate: function(instance, options, fn) {
          //instance.writeJSON();
          return fn(null, instance);
        }
      },
      instanceMethods: {
        writeJSON: function (){
          var deferred = Q.defer();
          /*
          var editorialID = this.EditorialId;
          var type = this.type;
          Feature.findAll({
                where: {"type": this.type, EditorialId: this.EditorialId},
                limit: 100,
                include: [{model: models.File}],
                order: [["startDate", "DESC"], ["endDate", "DESC"], ['createdAt', 'DESC']]
          }).then(function(results){
            var returns = [];

            __.forEach(results, function (result, index){
              returns.push(result.toJSON());
            })



            var folder = FuriousUtils.adjustIdPath(editorialID);
            var BasePath = path.join(__dirname, "..", "json");
            var completePath = FuriousUtils.generatePath(BasePath, folder)
            var json = JSON.stringify(returns);

            try {
              var dir = path.join(completePath, "EDITORIAL_"+editorialID+"_"+type+".json");
              fs.open(dir, "w+", function (err, fd){
                if(err){return deferred.reject(true); }
                fs.write(fd, json, function (err){
                  fs.close(fd, function(){
                    return deferred.resolve(true)
                  })
                });

              })
            } catch(e){
              return deferred.reject(true);
            }

          }).catch(function (err){
            deferred.reject(true);
          }) */
          deferred.resolve(true);
          return deferred.promise;
        }
      },
      classMethods: {
        associate: function (models){
          Feature.belongsToMany(models.File, {through: "FeaturedFile"})
          Feature.belongsTo(models.Editorial)
        }
      }
    })
  return Feature
}
