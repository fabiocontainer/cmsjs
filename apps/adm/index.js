var express = require("express");
var routes = require("./config/routes");
var adm = express();
var routeManager = require("../../lib/RouteManager")
var bodyParser = require("body-parser");

module.exports = function (configs){
  
  adm.use("/uploads", express.static(path.join(configs.initPath, 'uploads')));
  adm.use("/", express.static(path.join(__dirname, 'dreamz')));
  
  
  //adm.customParams = require("./config/params");
  //adm.policies = require("./config/policies");
  //adm.PATH = __dirname;
  //adm.set('views', __dirname + '/views');
  //adm.locals.dirPath = __dirname;
  //adm.set('view options', {layout: 'layouts/other'})


  

  //routeManager(api, routes);
  //routeManager(adm, routes)

  adm.use(function (err, req, res, next){
    var status = 404;
    if(err.status){
      status = err.status;
      delete err.status;
    }
    return res.status(status).json({error: err})
  })
  
  return adm;
}
