var express = require("express");
var routes = require("./config/routes");
var cors = require("cors");
var bodyParser = require("body-parser");
var api = express();
var routeManager = require("../../lib/RouteManager")
var multer = require("multer");
var moment  = require("moment")
var fs  = require("fs")
var path  = require("path")


//api.use(bodyParser.urlencoded({extended: true}));



module.exports = function (configs){

  api.customParams = require("./config/params");
  api.policies = require("./config/policies");

  //api.use("/uploads", function (req, res){
  //  res.redirect("http://homol.estrelando.com.br/uploads" + req.url);
  //})
  //api.use("/uploads", express.static(path.join(configs.initPath, 'uploads')));

  api.use(multer({
    dest: configs.initPath + '/uploads/',
    limits: {
      fieldSize: '50mb'
    },
    putSingleFilesInArray: true,
    rename: function(fieldname, filename, req, res){
      var cDate = moment().format('-X');
      return filename.replace(/\W+/g, '-').toLowerCase() + cDate;
    },
    changeDest: function (dest, req, res){
      var oldDir = dest;
      var nDir = dest;

      var cDate = moment().format('YYYY/MM/DD/');

      var arr = cDate.split("/");
      var path = ""

      for(var i in arr){
        try {
          path += arr[i] + "/";
          fs.lstatSync(nDir + path);
        } catch(err){
          fs.mkdirSync(nDir + path);
        }
      }
      return nDir + path;
    }
  }));
  api.use(bodyParser.json());
  api.use(bodyParser.urlencoded({ extended: false }));


  api.use(cors());
  app.PATH = __dirname;

  //api.set('views', __dirname + '/views');
  api.locals.dirPath = __dirname;
  //api.set('view options', {layout: 'layouts/other'})

  //routeManager(api, routes);
  routeManager(api, routes)

  api.use(function (err, req, res, next){
    var status = 404;
    if(err.status){
      status = err.status;
      delete err.status;
    }
    console.log(err);
    console.log("REQUEST BLOCKED");
    return res.status(status).json({error: err})
  })
  return api;
}
