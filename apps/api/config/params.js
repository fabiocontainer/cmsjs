var models  = require('../../../models');

module.exports = function (app){
  app.param("editorial_id", function (req, res, next, id){
    models.Editorial.find({where: {'id': id}}).then(function (editorial){
      if(editorial){ req.editorial = editorial; return next(); }
      return next("Failed to load Editorial");
    }).catch(function (err){
      return next("failled to load Editorial");
    })
  })
  app.param("content_id", function (req, res, next, id){
    models.Content.find({where: {'id': id}}).then(function (content){
      if(content){ req.content = content; return next(); }
      return next("Failed to load Content");
    }).catch(function (err){
      return next("failled to load Content");
    })
  })
  app.param("question_id", function (req, res, next, id){
    models.Question.find({where: {'id': id}}).then(function (question){
      if(question){ req.question = question; return next(); }
      return next("Failed to load question");
    }).catch(function (err){
      return next("failled to load question");
    })
  })
  app.param("site_id", function (req, res, next, id){
    models.Editorial.find({where: {'id': id, ParentId: null}}).then(function (site){
      if(site){ req.site = site; return next(); }
      return next("Failed to load Site");
    }).catch(function (err){
      return next("failled to load Site");
    })
  })
}