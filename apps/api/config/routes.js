module.exports = {
  /*
  "GET /health": {
    controller: "Home",
    action: "index",
    restrict: false
  },*/
  "GET /search": {
    controller: "Contents",
    action: "search",
    restrict: true
  },
  "POST /oauth/token": {
    controller: "Users",
    action: "login",
    restrict: false,
  },
  "GET /files/:id": {
    controller: "Files",
    action: "show",
    restrict: true,
  },
  "DELETE /files/:id": {
    controller: "Files",
    action: "remove",
    restrict: true,
  },
  "POST /files": {
    controller: "Files",
    action: "create",
    restrict: true,
  },
  "GET /files/:id": {
    controller: "Files",
    action: "show",
    restrict: true,
  },
  "GET /files": {
    controller: "Files",
    action: "index",
    restrict: true,
  },
  "GET /:site_id/specials": {
    controller: "Special",
    action: "index",
    restrict: true
  },
  "GET /:site_id/specials/:id": {
    controller: "Special",
    action: "show",
    restrict: true
  },
  "PUT /:site_id/specials/:id": {
    controller: "Special",
    action: "update",
    restrict: true
  },
  "DELETE /:site_id/specials/:id": {
    controller: "Special",
    action: "remove",
    restrict: true
  },
  "POST /:site_id/specials": {
    controller: "Special",
    action: "create",
    restrict: true
  },
  "PUT /:site_id/content/:content_id/votes": {
    controller: "Contents",
    action: "votes",
    restrict: true
  },
  "POST /:site_id/content/:content_id/questions/:question_id/answers": {
    controller: "Answer",
    action: "create",
    restrict: true
  },
  "GET /:site_id/content/:content_id/polls": {
    controller: "Poll",
    action: "index",
    restrict: true
  },
  "PUT /:site_id/content/:content_id/polls/:id": {
    controller: "Poll",
    action: "vote",
    restrict: true
  },
  "POST /:site_id/content/:content_id/polls": {
    controller: "Poll",
    action: "create",
    restrict: true
  },
  "GET /:site_id/content/:content_id/questions": {
    controller: "Questions",
    action: "index",
    restrict: true
  },
  "POST /:site_id/content/:content_id/questions": {
    controller: "Questions",
    action: "create",
    restrict: true
  },
  "PUT /:site_id/content/:id/updateJson": {
    controller: "Contents",
    action: "updateJson",
    restrict: true
  },
  "POST /:site_id/content/:content_id/results": {
    controller: "QuizResult",
    action: "create",
    restrict: true
  },
  "GET /:site_id/content/:content_id/results": {
    controller: "QuizResult",
    action: "index",
    restrict: true
  },
  "POST /:site_id/content/:content_id/gallery": {
    controller: "Gallery",
    action: "create",
    restrict: true
  },
  "PUT /:site_id/content/:id": {
    controller: "Contents",
    action: "update",
    restrict: true
  },
  "DELETE /:site_id/content/:id": {
    controller: "Contents",
    action: "remove",
    restrict: true
  },
  "POST /:site_id/content": {
    controller: "Contents",
    action: "create",
    restrict: true
  },
  "GET /:site_id/content/:id": {
    controller: "Contents",
    action: "show",
    restrict: true
  },
  "GET /:site_id/content": {
    controller: "Contents",
    action: "index",
    restrict: true
  },
  "GET /:site_id/editorials/:editorial_id/template": {
    controller: "Editorials",
    action: "template",
    restrict: true
  },
  "GET /:site_id/editorials/:editorial_id/css": {
    controller: "Editorials",
    action: "css",
    restrict: true
  },
  "GET /:site_id/analytics/visits": {
    controller: "Analytics",
    action: "visits",
    restrict: true
  },
  "GET /:site_id/analytics/keywords": {
    controller: "Analytics",
    action: "keywords",
    restrict: true
  },
  "GET /:site_id/analytics/pageviews": {
    controller: "Analytics",
    action: "pageviews",
    restrict: true
  },
  "GET /:site_id/homes": {
    controller: "Editorials",
    action: "homes",
    restrict: true
  },
  "GET /:editorial_id/editorials/:id": {
    controller: "Editorials",
    action: "show",
    restrict: true
  },
  "GET /editorials/:editorial_id/featureds": {
    controller: "Featured",
    action: "index",
    restrict: true
  },
  "GET /editorials/:editorial_id/featureds/starts": {
    controller: "Featured",
    action: "starts",
    restrict: true
  },
  "DELETE /:editorial_id/editorials/:id": {
    controller: "Editorials",
    action: "remove",
    restrict: true
  },
  "PUT /:editorial_id/editorials/:id": {
    controller: "Editorials",
    action: "update",
    restrict: true
  },
  "POST /:editorial_id/editorials": {
    controller: "Editorials",
    action: "create",
    restrict: true
  },
  "GET /:editorial_id/editorials": {
    controller: "Editorials",
    action: "index",
    restrict: true
  },
  "POST /editorials/:editorial_id/featureds/bulk": {
    controller: "Featured",
    action: "createBulk",
    restrict: true
  },
  "POST /editorials/:editorial_id/featureds": {
    controller: "Featured",
    action: "create",
    restrict: true
  },
  "GET /groups/:id" : {
    controller: "Group",
    action: "show",
    restrict: true
  },
  "PUT /groups/:id" : {
    controller: "Group",
    action: "update",
    restrict: true
  },
  "GET /groups": {
    controller: "Group",
    action: "index",
    restrict: true
  },
  "POST /groups" : {
    controller: "Group",
    action: "create",
    restrict: true
  },
  "GET /users/:id" : {
    controller: "Users",
    action: "show",
    restrict: true
  },
  "GET /users" : {
    controller: "Users",
    action: "index",
    restrict: true
  },
  "POST /users" : {
    controller: "Users",
    action: "create",
    restrict: true
  },
  "GET /apps/:id": {
    controller: "Apps",
    action: "show",
    restrict: true
  },
  "POST /apps": {
    controller: "Apps",
    action: "create",
    restrict: true
  },
  "PUT /apps/:id": {
    controller: "Apps",
    action: "update",
    restrict: true
  },
  "DELETE /apps/:id": {
    controller: "Apps",
    action: "remove",
    restrict: true
  },
  "GET /me": {
    controller: "Users",
    action: "me",
    restrict: true
  },
  "GET /sites": {
    controller: "Editorials",
    action: "sites",
    restrict: true
  },
  "GET /apps": {
    controller: "Apps",
    action: "index",
    restrict: true
  },
  "GET /tags/:id": {
    controller: "Tags",
    action: "show",
    restrict: true
  },
  "POST /tags": {
    controller: "Tags",
    action: "create",
    restrict: true
  },
  "PUT /tags/:id": {
    controller: "Tags",
    action: "update",
    restrict: true
  },
  "DELETE /tags/:id": {
    controller: "Tags",
    action: "remove",
    restrict: true
  },
  "GET /tags": {
    controller: "Tags",
    action: "index",
    restrict: true
  },
  "ALL /" : {
    controller: "Home",
    action: "index",
    restrict: true
  }
}
