var __ = require("underscore");
var models  = require('../../../models');

function verifyPermission(neededPermission, req, next){
  if(!req.user || req.user.Groups.length < 1) { return next({status: 403, message: "Invalid Credentials"}); }
  var hasPermission = false;
  __.forEach(req.user.Groups, function (group, index){
    if(group.Permissions && hasPermission == false){
      __.forEach(group.Permissions, function (permission, i){
        if(permission.role == neededPermission && hasPermission == false){
          hasPermission = true;
        }
      })
    }
  })
  
  if(hasPermission == false) { 
    console.log(neededPermission);
    return next({status: 403, message: "Missing Permission"});
  } else {
    return next();
  }
}

module.exports = function verifyCredentials(Permission){
  return function (req, res, next){
    if (!req.headers.authorization) { return next({status: 403, message: "Token Needed"})}
    var reg = new RegExp("Bearer (.+)");
    var matches = reg.exec(req.headers.authorization);
    if(matches && matches[1]){
      var t = matches[1];
      
      models.Token.find({where: {
        token: t,
        validUntil: {
          $gte: new Date()
        }
      }, include: [{model: models.User, include: {model: models.Group, include: models.Permission}}]}).then(function (token){
        if(!token) { return next({status: 403, message: "Invalid Credentials"}) }
        req.user = token.User;
        return verifyPermission(Permission, req, next);
      })
      
    } else {
      return next({status: 403, message: "Invalid Credentials"})
    }
  }
}
