var models  = require('../../../models');
var Apps = models.App
var User = models.User
var Token = models.Token

var bcrypt = require("bcrypt")
var __ = require("underscore");
var async = require("async")

module.exports = function (){
  var $this = this;

  this.allowedFields = function (req){
    var fields = ["name", "email", "password", "status", "GroupId"];
    var obj = {};

    for(var i in fields){
      var field = fields[i];
      if(req.body[field]){
        var value = req.body[field];
        if(__.isNumber(value)){
          obj[field] = Number(value);
        } else {
          obj[field] = value;
        }
      }
    }

    return obj;
  }
  // Show
  this.show = function (req, res){
    var id = parseInt(req.params.id);
    if(id){
      models.User.find({where: {'id': id}, include: [{all:true, nested: true}]}).then(function (result){
        if(result){
            return res.status(200).json(result).end();
         } else {
           return  res.status(404).json({message: "Register not found"}).end();
         }
      })
    }
  }
  // Index
  this.index = function (req, res, next){
    var page = (req.query.page)?parseInt(req.query.page):0;
    var perPage = (req.query.perPage)?parseInt(req.query.perPage):10;

    models.User.findAndCountAll({offset: page, limit: perPage}).then(function (result){
      return res.status(200).json(result).end();
    })
  }
  // Me
  this.me = function (req, res, next){
    if(req.user){
      return res.status(200).json(req.user).end();

    }
  }
  // Create
  this.create = function (req, res, next){
    var obj = $this.allowedFields(req);
    models.User.create(obj).then(function (user){
      user.reload().then(function (){
        return res.status(201).json(user).end();
      })

    }).catch(function (err){
      return res.status(406).json(err).end();
    })
  }


  this.login = function (req, res, next){
    console.log(req.body);
    if(req.body.email && req.body.pass && req.body.appName && req.body.appSecret){

      async.auto({

        loadApp: function (cb){
          Apps.find({where: {
            clientId: req.body.appName
          }}).then(function (res){
            if(!res){ return cb({error: "Invalid APP"}, null); }
            if(res.secret == req.body.appSecret){
              return cb(null, res);
            } else {
              return cb({error: "Invalid Client"}, null);
            }
          }).catch(function (err){
            return cb(((err)?err:"Invalid Client"), null)
          })
        },
        loadUser: function(cb){
          User.find({
            where: {
              email: req.body.email
            }
          }).then(function (user){
              if(!user){ return cb({error: "Invalid Passwrod or username"}, null); }
            	bcrypt.compare(req.body.pass, user.passwordHash, function (err, match){
            	  if(err){ return cb(err, null) }
                if(match){
                  return cb(null, user);
                } else {
                  return cb({error: "Invalid Password or Username"}, null);
                }
          	  })
          })
        },

        genToken: ['loadApp', 'loadUser', function (cb, result){
          if(result.loadApp && result.loadUser){
            var tokenBase = result.loadApp.clientId + "-" + result.loadUser.name + "" + (new Date());

            bcrypt.genSalt(10, function(err, salt){
							if(err) { return cb(err, null) }

							bcrypt.hash(tokenBase, salt, function(err, hash){
								if(err) { return cb(err, null)}
								Token.create({
								  'token': hash,
								  UserId: result.loadUser.id,
								  AppId: result.loadApp.id,
								}).then(function (token){
								  return cb(null, {type: "Bearer", 'token': token.token, "validUntil": token.validUntil});
								}).catch(function (err){
								  console.log(err);
								  return cb("Failed to create Token");
								})
							})
						})
          }
        }]
      }, function (err, result){
        console.log(err);

        if(err){ return res.status(500).json(err).end(); }
        return res.status(200).json(result.genToken).end();
      })
    } else {
      return res.status(403).end();
    }
  }
  return this;
}
