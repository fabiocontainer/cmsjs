var models  = require('../../../models');
var FilesContent = models.FilesContent

var __ = require("underscore");
var async = require("async")


function allowedFields (req){
  var fields = ["title", "description", "order","credits", "type", "fileSize", "FileId"];
  var obj = {};

  for(var i in fields){
    var field = fields[i];
    if(req[field]){
      var value = req[field];
      if(__.isNumber(value)){
        obj[field] = Number(value);
      } else {
        obj[field] = value;
      }
    }
  }

  return obj;
}
function formatRequest(params){
  var answers = []
  if(__.isArray(params)){
    __.forEach(params, function (item, index){
      answers.push(allowedFields(item))
    })
  } else {
    answers.push(allowedFields(params))
  }
  return answers;
}



module.exports = function (){
  var $this = this;

  this.create = function (req, res){
    var gallery = formatRequest(req.body)
    var returns = [];



    req.content.getFilesContents({where: {type: 'GALLERY'}}).then(function (associations){
      __.forEach(associations, function (item, index){
        item.destroy({force: true})
      })
    })

    async.eachLimit(gallery, 5, function (item, cb){
      item.ContentId = req.content.id;
      if(!item.type){
        item.type = "GALLERY"
      }

      FilesContent.create(item).then(function (association){
        returns.push(association);
        return cb(null, association);
      }).catch(function(err){
        return cb(err, null);
      })
    }, function (err){
      if(err || returns.length < 1) { return res.status(500).end(); }
      if(returns.length > 0){
        var cont = req.content.writeJSON();
        if(cont){
          cont.then(function (){
            req.content.writeSiteContent()
          })
        }


        //req.question.addFilesContents(returns).then(function (){
          return res.status(201).json(returns).end();
        //}).catch(function (){
        //  return res.status(500).end();
        //})
      } else {
        return res.status(500).end();
      }
    })
  }
  return this;
}
