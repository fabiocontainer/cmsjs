var models  = require('../../../models');
var Specials = models.Special

var __ = require("underscore");
var async = require("async")


function allowedFields (req){
  var fields = ["id", "title", "url", "color", "FileId", "titleSEO", "keywordsSEO", "descriptionSEO", "TagId"];
  var obj = {};
  
  for(var i in fields){
    var field = fields[i];
    if(req[field]){
      var value = req[field];
      if(__.isNumber(value)){
        obj[field] = Number(value);
      } else {
        obj[field] = value;
      } 
    }
  }
  
  return obj;
}

module.exports = function (){
  var $this = this;
  this.index = function(req, res){
    var page = (req.query.page)?parseInt(req.query.page):0;
    var perPage = (req.query.perPage)?parseInt(req.query.perPage):10;
    
    
    Specials.findAndCountAll({where: {SiteId: req.site.id}, offset: (perPage * page), limit: perPage}).then(function (results){
      return res.status(200).json(results).end();
    }).catch(function (e){
      return res.status(406).json(e).end();
    })
  }
  this.create = function (req, res){
    var special = allowedFields(req.body)
    special.SiteId = req.site.id;
    Specials.create(special).then(function(item){
      item.writeJSON();
      return res.status(201).json(item).end();
    }).catch(function(err){
        return res.status(500).json(err).end();
    })
  }
  this.update = function (req, res){
    var special = allowedFields(req.body)
    special.SiteId = req.site.id;
    
    Specials.find({where: {id: req.params.id}, include: [models.File, models.Tag]}).then(function(item){
      if(!item){ return res.status(404).json({message: "Content not found"}).end(); }
      __.forEach(special, function (value, prop){
        item[prop] = value;
      })
      item.save().then(function(item){
       Specials.writeJson();
        
        return res.status(201).json(item).end(); 
      }).catch(function(err){
          return res.status(500).json(err).end();
      })
    })
  }
  this.remove = function (req, res, next){
    Specials.find({
      where: {
        id: req.params.id,
        SiteId: req.site.id
    }}).then(function (content){
      return content.destroy()
    }).then(function (content){
      return res.status(201).end();
    }).catch(function (err){
      return next({err: 500, message: "Unable to delete content"})
    })
  }
  this.show = function (req, res){
    Specials.find({where: {id: req.params.id}, include: [models.File, models.Tag]}).then(function (result){
      if(!result) { return res.status(404).json({message: "Special not found"}).end(); }
      return res.status(200).json(result).end();
    }).catch(function (err){
      return res.status(500).json(err).end();
    })
  }
  return this;
}
