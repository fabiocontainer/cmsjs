var __ = require("underscore");
var models  = require('../../../models');
var Apps = models.App

function allowedFields(params){
  var obj = {};
  var fields = ["name", "status"];
  __.forEach(params, function (value, index){
    if(fields.indexOf(index) >= 0){
      obj[index] = value;
    }
  })
  return obj;
}

module.exports = function (){
  var $this = this;
  this.show = function (req, res){
    var id = req.params.id
    Apps.find({where: {"id": id}}).then(function (result){
      var app = result.toJSON();
      delete app.secret;
      delete app.clientId;
      res.status(200).json(app)
    }).catch(function(err){
      res.status(500).end();
    })
  }
  this.create = function (req, res){
    var params = allowedFields(req.body);
    Apps.create(params).then(function (app){
      res.status(202).json(app).end();
    }).catch(function (err){
      res.status(406).json(err).end();
    })
  }
  
  // 
  this.update = function (req, res){
    var id = req.params.id;
    var params = allowedFields(req.body);
    Apps.find({where: {"id": id}}).then(function (app){
      if(!app){ return res.status(404).end();}
      
      __.forEach(params, function (value, index){
        app[index] = value;
      })
      app.save().then(function(){
        res.status(202).end();
      }).catch(function (err){
        res.status(406).json(err).end();
      })
    })
  }
  this.remove = function (req, res){
    var id = req.params.id;
    Apps.find({where: {"id": id}}).then(function (app){
      if(!app){ return res.status(404).end();}
      app.destroy().then(function(){
        res.status(202).end();
      }).catch(function (){
        res.status(503).end();
      })
    })
  }
  this.index = function (req, res){
    var page = (req.param.page)?parseInt(req.param.page):0;
    Apps.findAndCountAll({offset: page, limit: 10}).then(function (result){
      res.status(200).json(result)
    }).catch(function(err){
      res.status(500).end();
    })
  }
  return this;
}
