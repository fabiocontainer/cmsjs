var models  = require('../../../models');
var Pools = models.Pool

var __ = require("underscore");
var async = require("async")
var Q = require("q");


function allowedFields (req){
  var fields = ["id", "title", "votes", "fullText", "FileId"];
  var obj = {};
  
  for(var i in fields){
    var field = fields[i];
    if(req[field]){
      var value = req[field];
      if(__.isNumber(value)){
        obj[field] = Number(value);
      } else {
        obj[field] = value;
      } 
    }
  }
  
  return obj;
}
function formatRequest(params){
  var questions = []
  if(__.isArray(params)){
    __.forEach(params, function (item, index){
      questions.push(allowedFields(item))
    })
  } else {
    questions.push(allowedFields(params))
  }
  return questions;
}



module.exports = function (){
  var $this = this;
  this.index = function(req, res){
    req.content.getPools({include: [models.File]}).then(function (results){
      return res.status(200).json(results).end();
    }).catch(function (e){
      return res.status(406).json(e).end();
    })
  }
  
  this.vote = function (req, res){
    req.content.getPools({where: {id: req.params.id}}).then(function (polls){
      if(polls[0]){
        var poll = polls[0];
        poll.votes++;
        return poll.save()
      }
    }).then(function(poll){
      return req.content.writeJSON()
    }).then(function (){
      return req.content.getEditorials().then(function (editorials){
        var deferred = Q.defer();
        async.eachLimit(editorials, 2, function (edt, cb){
          edt.writeContentTypeJson(req.content.type).then(function (){
            cb();
          }).catch(function (){
            cb();
          })
        }, function (){
          deferred.resolve(true);
        })
          
        return deferred.promise;
      })
    }).then(function (){
      console.log("WROTE RESPONSE");
      return res.status(201).end();
    }).catch(function (){
      return res.status(500).end();
    })
  }
  this.create = function (req, res){
    var questions = formatRequest(req.body)
    var nQuestions = [];
    
    req.content.getPools().then(function (pools){
      __.forEach(pools, function (pool){
        
        pool.destroy();
        
        //question.getAnswers().then(function(answers){
        //  __.forEach(answers, function (answer, index){
        //    answer.destroy({force: true});
        //  })
        //  question.destroy({force: true})
        //}).catch(function (){
        //  question.destroy({force: true})
        //})
      })
    })
    
    __.forEach(questions, function (item, index){
      questions[index]['ContentId'] = req.content.id;
    })
    
    async.eachLimit(questions, 5, function (item, cb){

      Pools.create(item).then(function (poll){
        nQuestions.push(poll);
        return cb(null, poll);
      }).catch(function(err){
        return cb(err, item);
      })
    }, function (err){
      if(err || nQuestions.length < 1) { return res.status(500).json(err).end(); }
      if(nQuestions.length > 0){
        //req.content.setQuestions(nQuestions).then(function (){
          return res.status(201).json(nQuestions).end();
        //}).catch(function (){
        //  return res.status(500).end();
        //})
      } else {
        return res.status(500).end();
      }
    })
  } 
  return this;
}
