var models  = require('../../../models');
var Results = models.QuizResult

var __ = require("underscore");
var async = require("async")


function allowedFields (req){
  var fields = ["title", "fullText", "FileId"];
  var obj = {};
  
  for(var i in fields){
    var field = fields[i];
    if(req[field]){
      var value = req[field];
      if(__.isNumber(value)){
        obj[field] = Number(value);
      } else {
        obj[field] = value;
      } 
    }
  }
  
  return obj;
}
function formatRequest(params){
  var questions = []
  if(__.isArray(params)){
    __.forEach(params, function (item, index){
      questions.push(allowedFields(item))
    })
  } else {
    questions.push(allowedFields(params))
  }
  return questions;
}



module.exports = function (){
  var $this = this;
  this.index = function (req, res){
    req.content.getQuizResults({include: [models.File]}).then(function (result){
        res.status(200).json(result).end();
    }).catch(function (err){
      res.status(500).json(err).end();
    })
  }
  this.create = function (req, res){
    var questions = formatRequest(req.body)
    var nQuestions = [];
    
    req.content.getQuizResults().then(function (questions){
      __.forEach(questions, function (question){
        question.destroy({force: true})
      })
    })
    
    
    async.eachLimit(questions, 5, function (item, cb){

      Results.create(item).then(function (question){
        nQuestions.push(question);
        return cb(null, question);
      }).catch(function(err){
        return cb(err, item);
      })
    }, function (err){
      if(err || nQuestions.length < 1) { return res.status(500).end(); }
      if(nQuestions.length > 0){
        req.content.setQuizResults(nQuestions).then(function (){
          return res.status(201).json(nQuestions).end();
        }).catch(function (){
          return res.status(500).end();
        })
      } else {
        return res.status(500).end();
      }
    })
  } 
  return this;
}
