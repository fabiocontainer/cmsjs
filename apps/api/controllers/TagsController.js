var __ = require("underscore");
var models  = require('../../../models');
var Tags = models.Tag;
var Files = models.File;

function allowedFields(params){
  var obj = {};
  var fields = ["title","slug","job","taxonomy","profileDate","profileType","profileDeathDate", "fileId"];
  __.forEach(params, function (value, index){
    if(fields.indexOf(index) >= 0){
      obj[index] = value;
    }
  })
  return obj;
}

module.exports = function (){
  var $this = this;
  this.show = function (req, res){
    var id = req.params.id
    Tags.find({where: {"id": id}}).then(function (result){
      res.status(200).json(result)
    }).catch(function(err){
      res.status(500).end();
    })
  }
  this.create = function (req, res){
    var params = allowedFields(req.body);
    if(params.fileId){
      var fileId = params.fileId;
      delete params.fileId;
    }
    console.log(params);
    
    Tags.create(params).then(function (result){
      if(!fileId) { return res.status(201).json(result).end(); }
      Files.find({where: {'id': fileId}}).then(function (file){
        result.setFile(file).done(function(){
          result.writeJSON();
          
          return res.status(201).json(result).end();
        })
      }).catch(function(){
        return res.status(202).json(result).end();
      })
      
    }).catch(function (err){
      res.status(406).json(err).end();
    })
  }
  this.update = function (req, res){
    var id = req.params.id;
    var params = allowedFields(req.body);
    if(params.fileId){
      var fileId = params.fileId;
      delete params.fileId;
    }
    Tags.find({where: {"id": id}}).then(function (tag){
      if(!tag){ return res.status(404).end();}
      
      __.forEach(params, function (value, index){
        tag[index] = value;
      })
      
      tag.save().then(function(){
        tag.writeJSON();
        if(!fileId) { return res.status(201).end(); }
        Files.find({where: {'id': fileId}}).then(function (file){
          if(!file){ return res.status(202).end() }
          tag.setFile(file).done(function(){
            return res.status(201).end();
          })
        }).catch(function(){
          return res.status(202).end();
        })
        
      }).catch(function (err){
        res.status(406).json(err).end();
      })
    })
  }
  this.remove = function (req, res){
    var id = req.params.id;
    Tags.find({where: {"id": id}}).then(function (tag){
      if(!tag){ return res.status(404).end();}
      tag.destroy().then(function(){
        res.status(204).end();
      }).catch(function (){
        res.status(503).end();
      })
    })
  }
  this.index = function (req, res){
    var page = (req.query.page)?parseInt(req.query.page):0;
    var perPage = (req.query.perPage)?parseInt(req.query.perPage):10;
    var params = {offset: (page*perPage), limit: perPage};
    if(req.query.name) { 
      if(!params.where){
        params.where = {};
      }
      params.where.title = {$like: req.query.name+"%"}
    }
    if(req.query.q) { 
      if(!params.where){
        params.where = {};
      }
      params.where.title = {$like: "%"+req.query.q+"%"}
    }
    if(req.query.type) { 
      if(!params.where){
        params.where = {};
      }
      params.where.taxonomy = (req.query.type == "PROFILE")?"ART_PROFILE":"TAG";
    }
    params.order = [['title', 'ASC']];
    Tags.findAndCountAll(params).then(function (result){
      if(req.query.name && req.query.type){
        return res.status(200).json(result.rows).end();
      }
      return res.status(200).json(result)
    }).catch(function(err){
      
      res.status(500).end();
    })
  }
  return this;
}
