var __ = require("underscore");
var models  = require('../../../models');
var Files = models.File;
var moment = require("moment");
var fs = require("fs");
var path = require("path");
var Q = require("q");


module.exports = function (){
  this.create = function (req, res, next){
    var files = [];    
    __.forEach(req.files.files, function (file, index){
      
      files.push({
          filename: file.name,
    			fileType: "FILE",
    			extension: file.extension 
    	})
    })    
    if(req.body.urls){
      if(__.isArray(req.body.urls)){
        __.forEach(req.body.urls, function (url){
          files.push({
              filename: url,
        			fileType: "URL" 
        	})
        })
      }
    }
    if(files.length > 0){
      var queues = [];
      __.forEach(files, function (item){
        console.log(item);
        queues.push(Files.create(item));
      })
      
      var promise = Q.all(queues)
      
      promise.then(function (){
        return res.status(201).json(queues).end();
      }).catch(function (e){
        console.log(e);
        res.status(406).end();
      })
      
      //Files.bulkCreate(files).then(function (filesNew){
      //  res.status(201).json(filesNew).end();
      //})
    } else {
      res.status(406).end();
    }
  }
  this.index = function (req, res, next){
    
    var page = (!parseInt(req.query.page))?0:parseInt(req.query.page);
    var perPage = (!parseInt(req.query.perPage))?10:parseInt(req.query.perPage);
    
    var criterias = {offset: (perPage * page), limit: perPage, order: 'createdAt DESC'};
    
    if(req.query.q){
      criterias.where = {filename: {$like: req.query.q+"%"}};
    }
    if(req.query.type){
      if(!criterias.where){
        criterias.where = {};
      }
      criterias.where.fileType = req.query.type;
    }
    
    Files.findAndCountAll(criterias).then(function (result){
      return res.status(200).json(result).end();
    }).catch(function (err){
      return next({status: 500, message: "Error getting files"});
    })
  }
  this.show = function (req, res, next){
    
    Files.find(req.params.id).then(function (file){
      if(!file) { return next({status: 404, message: 'File not Found'})}
      res.status(200).json(file).end();
    }).catch(function (){
      return next({status: 500, message: 'Error getting File'})
    })
  }
  this.remove = function (req, res, next){
    Files.find(req.params.id).then(function (file){
      if(!file) { return next({status: 404, message: 'File not Found'})}
      return file.destroy();
    }).catch(function (){
      return next({status: 500, message: 'Error deleting File'})
    }).done(function (){
      return res.status(201).end();
    })
  }
  
  return this;
}
