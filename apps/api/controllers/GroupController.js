var __ = require("underscore");
var models  = require('../../../models');
var Groups = models.Group;
var Permissions = models.Permission;
var Users = models.User;
var async = require("async");

function allowedFields(params){
  var obj = {};
  var allowed = ["name", "users_ids", "permissions_ids"];
  
  __.forEach(params, function(item, index){
    if(allowed.indexOf(index) >= 0){
      switch(index){
        case "name":
          obj.name = item;
        break
        default:
          obj[index] = [];
          if(__.isArray(item)){
            __.forEach(item, function (val){
              if(__.isNumber(val)){
                (obj[index]).push(Number(val));
              }
            })
          } else {
            if(__.isNumber(item)){
              (obj[index]).push(Number(item));
            }
          }
        break;
      }
    }
  })
  return obj
}
module.exports = function (){
  var $this = this;
  
  this.index = function (req, res){
    var page = (req.param.page)?parseInt(req.param.page):0;
    models.Group.findAndCountAll({offset: page, limit: 10}).then(function (result){
      res.status(200).json(result).end();
    }).catch(function (){
      res.status(503).end();
    })
  }
  this.show = function (req, res){
    var id = req.params.id;
    Groups.find({where: {'id': id}, include: [Users]}).then(function (group){
      return res.status(200).json(group).end();
    }).catch(function(){
      return res.status(404).end();
    })
  }
  this.create = function (req, res){
    var obj = allowedFields(req.body);
    var users_ids = [];
    if(obj.users_ids){
      users_ids = obj.users_ids;
      delete obj.users_ids;
    }
    var permissions_ids = []
    if(obj.permissions_ids){
      permissions_ids = obj.permissions_ids;
      delete obj.permissions_ids;
    }
    
    var plays = {
      createGroup: function (cb){
        Groups.create(obj).then(function (group){
          return cb(null, group);
        }).catch(function (err){
          return cb(err);
        })
      }
    }
    if(permissions_ids){
      plays.loadPermissions = function (cb, result){
        Permissions.findAll({where: {id: {$in: permissions_ids}}}).then(function (permissions){
          return cb(null, permissions);
        }).catch(function (err){
          return cb(null, []);
        })
      }
      
      plays.associatePermissions = ["createGroup", "loadPermissions", function (cb, results){
        if(results.createGroup && results.loadPermissions){
          console.log(results.loadPermissions);
          results.createGroup.setPermissions(results.loadPermissions).then(function (result){
            return cb(null, result)
          })
        } else {
          return cb(null, true);
        }
      }]
    }
    if(users_ids){
      plays.loadUsers = function (cb, result){
        Users.findAll({where: {id: {$in: users_ids}}}).then(function (users){
          return cb(null, users);
        }).catch(function (err){
          return cb(null, []);
        })
      }
      
      plays.associateUsers = ["createGroup", "loadUsers", function (cb, results){
        if(results.createGroup && results.loadUsers){
          results.createGroup.setUsers(results.loadUsers).then(function (result){
            return cb(null, result)
          })
        } else {
          return cb(null, true);
        }
      }]
    }
    async.auto(plays, function (err, results){
      if(err){ return res.status(503).end();}
      return res.status(202).json(results.createGroup)
    })
    
  }
  this.update = function (req, res){
    var id = req.params.id;
    var obj = allowedFields(req.body);
    if(obj.users_ids){
      var users_ids = obj.users_ids;
      delete obj.users_ids;
    }
    var permissions_ids = []
    if(obj.permissions_ids){
       permissions_ids = obj.permissions_ids;
      delete obj.permissions_ids;
    }
    
    Groups.find({where: {'id': id}, include: [Users]}).then(function (group){
      
      var play = {
        updateValues: function(cb){
          for(var i in obj){
            group[i] = obj[i];
          }
          
          group.save().then(function (group){
            cb(null, group);
          }).catch(function (err){
            cb(err);
          })
        }
      }
      
      
      if(permissions_ids){
        play.loadPermissions = ['updateValues', function (cb, result){
          Permissions.findAll({where: {id: {$in: permissions_ids}}}).then(function (permissions){
            return cb(null, permissions);
          }).catch(function (err){
            return cb(null, []);
          })
        }]
        
        play.associatePermissions = ["updateValues", "loadPermissions", function (cb, results){
          if(results.updateValues && results.loadPermissions){
            results.updateValues.setPermissions(results.loadPermissions).then(function (result){
              return cb(null, result)
            })
          } else {
            return cb(null, true);
          }
        }]
      }
      if(users_ids){
        play.loadUsers = ['updateValues', function (cb, result){
          Users.findAll({where: {id: {$in: users_ids}}}).then(function (users){
            return cb(null, users);
          }).catch(function (err){
            return cb(null, []);
          })
        }]
        
        play.associateUsers = ["updateValues", "loadUsers", function (cb, results){
          if(results.updateValues && results.loadUsers){
            results.updateValues.setUsers(results.loadUsers).then(function (result){
              return cb(null, result)
            })
          } else {
            return cb(null, true);
          }
        }]
      }
      
      async.auto(play, function(err, result){
        if(err){ return res.status(503).end(); }
        return res.status(202).end();
      })
      
      
      
    }).catch(function(){
      return res.status(404).end();
    })
  }
  
  this.remove = function (req, res){
    var id = req.params.id;
    Groups.find({where: {'id': id}}).then(function (group){
      group.destroy().then(function (){
        return res.status(202).json(group).end();
      }).catch(function (){
        return res.status(503).end();
      })
    }).catch(function(){
      return res.status(404).end();
    })
  }
  return this;
}
