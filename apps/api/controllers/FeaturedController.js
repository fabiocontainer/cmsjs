var __ = require("underscore");
var models  = require('../../../models');
var Feature = models.Feature;
var Files = models.File;
var Editorials = models.Editorial;
var async = require("async");

function strip_tags (input, allowed) {
     allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
     var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
         commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
     return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
         return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
     });
 }

function allowedFields(params){
  var obj = {};
  var fields = ["title", "type", "link", "intro", "fullText", "startDate", "endDate", "fileIds", "style"];
  __.forEach(params, function (value, index){
    if(fields.indexOf(index) >= 0){
      if(index == "title"){
        obj[index] = strip_tags(value, "<b> <i> <strong> <em>");
      } else {
        obj[index] = value;
      }

    }
  })
  return obj;
}
function prepareBulk(params){
  var obj = [];

  if(params.startDate){
    var startDate = params.startDate;
    delete params.startDate;
  }
  if(params.endDate){
    var endDate = params.endDate;
    delete params.endDate;
  }

  __.forEach(params, function (value, index){
    if(!__.isArray(value)){
      if(startDate){
        value.startDate = startDate;
      }
      if(endDate){
        value.endDate = endDate;
      }
      if(!value.type){
        value.type = index;
      }
      obj.push(allowedFields(value))
    } else {
      __.forEach(value, function (item, i){
        if(startDate){
          item.startDate = startDate;
        }
        if(endDate){
          item.endDate = endDate;
        }
        if(!obj.type){
          item.type = index;
        }
        obj.push(allowedFields(item))
      })
    }

  })
  return obj;
}

module.exports = function (){
  this.index = function (req, res, next){
    var params = {order: [['type', 'ASC'], ['startDate', 'DESC'], ['endDate', 'DESC']], include: [Files]};

    if(req.query.startDate){
      params.where = {'startDate': req.query.startDate};
    }

    req.editorial.getFeatures(params).then(function (results){
      return res.status(200).json({data: results}).end();
    }).catch(function (err){
      return next(err);
    })
  }
  this.starts = function (req, res, next){
    req.editorial.getFeatures({order: [['type', 'ASC'], ['startDate', 'DESC'], ['endDate', 'DESC']], group: ['startDate'], attributes: ["startDate"], limit: 20}).then(function (results){
      return res.status(200).json({data: results}).end();
    }).catch(function (err){
      return next(err);
    })
  }
  this.create = function (req, res, next){
    var obj = allowedFields(req.body);

    Feature.create(obj).then(function (result){
      result.setEditorial(req.editorial).then(function (){
        return res.status(201).end();
      })
    }).catch(function(){
      return next("Failed to create");
    })
  }
  this.createBulk = function (req, res, next){
    var featureds = prepareBulk(req.body);

    var cFeatureds = [];
    var lastType = "";

    async.eachLimit(featureds, 5, function (item, cb){
      if(item.fileIds){
        var filesIds = item.fileIds;
        delete item.fileIds;
      }
      item.EditorialId = req.editorial.id;

      Feature.create(item).then(function (feature){
        //feature.setEditorial(req.editorial).then(function (){
          if( lastType != feature.type ){
            lastType = feature.type;
            cFeatureds.push(feature)
          }

          if(!filesIds){ return cb(); }

          Files.findAll({where: {id: {$in: filesIds}}}).then(function (files){
            feature.setFiles(files).then(function (){
              //console.log(files);
              return cb();
            });
          })

        //})
      })
    }, function (err){
      if(err) { return res.status(500).end(); }
      if(cFeatureds.length > 0){
        //console.log(cFeatureds);

        for(var i in cFeatureds){
          if(cFeatureds[i]){
            //console.log(cFeatureds[i]['type'])
            var item = cFeatureds[i]
            //item.writeJSON();
          }
        }
      }
      return res.status(201).end();
    })
  }

  return this;
}
