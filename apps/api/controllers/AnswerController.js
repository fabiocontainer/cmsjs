var models  = require('../../../models');
var Answers = models.Answer

var __ = require("underscore");
var async = require("async")


function allowedFields (req){
  var fields = ["title", "fullText", "points", "FileId"];
  var obj = {};
  
  for(var i in fields){
    var field = fields[i];
    if(req[field]){
      var value = req[field];
      if(__.isNumber(value)){
        obj[field] = Number(value);
      } else {
        obj[field] = value;
      } 
    }
  }
  
  return obj;
}
function formatRequest(params){
  var answers = []
  if(__.isArray(params)){
    __.forEach(params, function (item, index){
      answers.push(allowedFields(item))
    })
  } else {
    answers.push(allowedFields(params))
  }
  return answers;
}



module.exports = function (){
  var $this = this;
  
  this.create = function (req, res){
    var answers = formatRequest(req.body)
    var returns = [];
    
    req.question.getAnswers().then(function (answers){
      __.forEach(answers, function (answer, index){
        answer.destroy({force: true})
      })
    })
    
    
    if(req.content.quizType == "correct"){
      for(var i in answers){
        answers[i]['points'] = (i == 0)?10:0;
      }
    } else {
      for(var i in answers){
        answers[i]['points'] = (i*10);
      }
    }
    
    async.eachLimit(answers, 5, function (item, cb){

      Answers.create(item).then(function (answer){
        returns.push(answer);
        return cb(null, answer);
      }).catch(function(err){
        return cb(err, null);
      })
    }, function (err){
      if(err || returns.length < 1) { return res.status(500).end(); }
      if(returns.length > 0){
        req.question.setAnswers(returns).then(function (){
          return res.status(201).json(returns).end();
        }).catch(function (){
          return res.status(500).end();
        })
      } else {
        return res.status(500).end();
      }
    })
  } 
  return this;
}
