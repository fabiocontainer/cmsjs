var moment = require("moment");
var path = require("path");

var pullAnalytics = require("ga-analytics");

module.exports = function (){
  this.visits = function (req, res, next){
    pullAnalytics({
      dimensions: "ga:date",
	    metrics: "ga:visits",
	    'start-date': moment().subtract(30, 'days').format("YYYY-mm-dd"),
	    'end-date': moment().format("YYYY-mm-dd"),
	    'limit': 10, 
	    clientId: "27799708581.apps.googleusercontent.com",
      serviceEmail: "27799708581@developer.gserviceaccount.com",
      key: path.join(__dirname, "..", "config/google-services-private-key.pem"),
      ids: "ga:9707931"
	  }, function (err, result){
	    if(err) { throw err; }
	    res.status(200).json(result);
	  })
	  
  }
  
  this.keywords = function (req, res, next){
    pullAnalytics({
	    dimensions: "ga:keyword",
	    metrics: "ga:visits",
	    'start-date': moment().subtract(30, 'days').format("YYYY-mm-dd"),
	    'end-date': moment().format("YYYY-mm-dd"),
	    'limit': 10, 
	    sort: '-ga:visits',
	    clientId: "27799708581.apps.googleusercontent.com",
      serviceEmail: "27799708581@developer.gserviceaccount.com",
      key: path.join(__dirname, "..", "config/google-services-private-key.pem"),
      ids: "ga:9707931"
	  }, function (err, result){
	    if(err) { throw err; }
	    res.status(200).json(result);
	  })
  }
  this.pageviews = function  (req, res, next){
    pullAnalytics({
	    dimensions: "ga:pagepath",
	    metrics: "ga:pageviews",
	    sort: '-ga:pageviews',
	    'start-date': moment().subtract(30, 'days').format("YYYY-mm-dd"),
	    'end-date': moment().format("YYYY-mm-dd"),
	    'limit': 10, 
	    clientId: "27799708581.apps.googleusercontent.com",
      serviceEmail: "27799708581@developer.gserviceaccount.com",
      key: path.join(__dirname, "..", "config/google-services-private-key.pem"),
      ids: "ga:9707931"
	  }, function (err, result){
	    if(err) { throw err; }
	    res.status(200).json(result);
	  })
  }
  return this;
}