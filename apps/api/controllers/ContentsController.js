// Libs
var __ = require("underscore");
var moment = require("moment");
var async = require("async");

// Models
var models  = require('../../../models');
var Contents = models.Content
var Files = models.File
var Editorials = models.Editorial
var FilesContent = models.FilesContent
var Tags = models.Tag




function allowedFields(params){
  var obj = {};
  var fields = ["title", "intro","slug","type","summary","fullText","writer","writerEmail","titleSEO","keywordsSEO","descriptionSEO","status","quizType","publishedAt", "showHome"];
  __.forEach(params, function (value, index){
    if(fields.indexOf(index) >= 0){
      obj[index] = value;
    }
  })


  return obj;
}

module.exports = function (){
  this.index = function (req, res, next){
    var page = (!parseInt(req.query.page))?0:parseInt(req.query.page);
    var perPage = (!parseInt(req.query.perPage))?10:parseInt(req.query.perPage);
    var format = (!req.query.isArray)?"object":"array";
    var criterias = {where: {SiteId: req.site.id}, offset: (perPage * page), limit: perPage, order: [['publishedAt', 'DESC']]};

    if(req.query.q){
      if(/id:([0-9]+)/i.exec(req.query.q)){
        criterias.where.id = /id:([0-9]+)/i.exec(req.query.q)[1];
      } else if(/text:(.+)/i.exec(req.query.q)) {
        criterias.where.fullText = {$like: "%"+(/text:(.+)/i.exec(req.query.q)[1])+"%"};
      } else {
        criterias.where.title = {$like: "%"+req.query.q+"%"};
      }

    }
    if(req.query.type){
      if(!criterias.where){
        criterias.where = {};
      }
      criterias.where.type = req.query.type.toLowerCase();
    }

    if(req.query.onlyPublished){
      criterias.where.status = 'published';
    }
    if(req.query.includeMedia){
      criterias.include = [{model: models.FilesContent, include: [models.File]}]
    }
    Contents.findAndCountAll(criterias).then(function (result){
      if(format == "object"){
        return res.status(200).json(result).end();
      } else {
        return res.status(200).json(result.rows).end();
      }
    }).catch(function (err){
      return next({status: 500, message: "Error Content files"});
    })
  }
  this.updateJson = function (req, res, next){
    Contents.find({
      where: {
        id: req.params.id,
        SiteId: req.site.id
      }
    }).then(function (content){
      //return content.writeJSON().then(function (){
        return res.status(200).json(content).end();
      //})
    }).catch(function (err){
      return next({err: 404, message: "Unable to load content"})
    })
  }
  this.show = function(req, res, next){
    Contents.find({
      where: {
        id: req.params.id,
        SiteId: req.site.id
      },
      include: [{model: FilesContent, include: [Files]}, Tags, Editorials]
    }).then(function (content){
      content.getExtras().then(function (){
        return res.status(200).json(content).end();
      })
    }).catch(function (err){
      return next({err: 404, message: "Unable to load content"})
    })
  }
  this.update = function (req, res, next){
    var obj = allowedFields(req.body);
    obj.SiteId = req.site.id;

    if(parseInt(req.body.thumbnailID)){
      var thumbnailID = parseInt(req.body.thumbnailID);
      var thumbnailCredit = (!req.body.thumbnailCredit)?"Divulgação":req.body.thumbnailCredit;
      var thumbnailTitle = (!req.body.thumbnailTitle)?"":req.body.thumbnailTitle;
      var thumbnailText = (!req.body.thumbnailText)?"":req.body.thumbnailText;
      var thumbnailSize = (!req.body.thumbnailSize)?"":req.body.thumbnailSize;
      var thumbnail = Files.find(thumbnailID);
    }
    if(req.body.tags_ids && req.body.tags_ids.length > 0){
      var tags = Tags.findAll({where: {id: {$in: req.body.tags_ids}}});
    }
    if(req.body.editorials_ids){
      var editorials = Editorials.findAll({where: {id: {$in: req.body.editorials_ids}}})
    }
    if(req.body.extras_ids){
      var contentsExtras = Contents.findAll({where: {id: {$in: req.body.extras_ids}}})
    }

    var oldTags = [];
    var oldEdts = [];

    var plays = {
      saveContent: function (cb){
        Contents.find({
          where: {
            id: req.params.id,
            SiteId: req.site.id
          }, include: [Tags, Editorials]
        }).then(function (content){
          oldTags = content.Tags;
          oldEdts = content.Editorials;

          __.forEach(obj, function (value, index){
            content[index] = value;
          })
          return content.save()
        }).then(function(content){

          return cb(null, content)
        }).catch(function (err){
          return cb(err, null)
        })
      }
    }

    if(tags){
      plays.saveTags = ['saveContent', function (cb, result){
        tags.then(function (tagsFinded){
          result.saveContent.setTags(tagsFinded).then(function (){


            var itensRemoved = oldTags.filter(function(item){
              var removed = true;
              for(var i in tagsFinded){
                if(item.id == tagsFinded[i]['id']){
                  removed = false;
                }
              }
              return removed;
            })

            __.forEach(tagsFinded, function (tag, index){
              tag.writeContentJSON(result.saveContent.SiteId);
            })
            __.forEach(itensRemoved, function (tag, index){
              tag.writeContentJSON(result.saveContent.SiteId);
            })

            return cb(null, true)
          }).catch(function (err){
            //console.log("PASSOU2")
            return cb(err, null)
          })
        }).catch(function (err){
          //console.log("PASSOU3")
          return cb(err, null)
        })
      }]
    }

    if(thumbnail) {
      plays.thumbnails = ['saveContent', function(cb, result){
        thumbnail.then(function (file){

         result.saveContent.getFilesContents({where: {type: 'THUMBNAIL'}}).then(function (associations){
            __.forEach(associations, function (item, index){
              item.destroy({force: true})
            })
            // File Create
            FilesContent.create({
              type: "THUMBNAIL",
              FileId: file.id,
              ContentId: result.saveContent.id,
              credits: thumbnailCredit,
              fileSize: thumbnailSize,
              title: thumbnailTitle,
        			description: thumbnailText
            }).then(function (thumbnail){
              return cb(null, thumbnail);
            }).catch(function (err){

              return cb(err, null);
            })

          })

        })
      }]
    }

    if(contentsExtras){

      plays.extras = ['saveContent', function (cb, result){
        contentsExtras.then(function (contents){

          result.saveContent.setChildren([]).then(function (){
            async.eachLimit(contents, 1, function (item, cb2){
              result.saveContent.addChildren(item).then(function (){
                return cb2(null, item);
              })
            }, function (err){
              return cb(null, contents)
            })

          })
        }).catch(function (err){
          return cb(err, null);
        })
      }]
    }

    if(editorials) {
      plays.editorials = ['saveContent', function(cb, result){
        editorials.then(function (editiorialsFinded){

          result.saveContent.setEditorials(editiorialsFinded).then(function (){

             var itensRemoved = oldEdts.filter(function(item){
                var removed = true;
                for(var i in editiorialsFinded){
                  if(item.id == editiorialsFinded[i]['id']){
                    removed = false;
                  }
                }
                return removed;
              })

              __.forEach(editiorialsFinded, function (editorial, index){
                if(result.saveContent.status == "published"){
                  editorial.contentEditorialsPath();
                  editorial.writeContentTypeJson(result.saveContent.type);
                }

              })
              __.forEach(itensRemoved, function (editorial, index){
                if (result.saveContent.status == "published") {
                  editorial.contentEditorialsPath();
                  editorial.writeContentTypeJson(result.saveContent.type);
                }
              })


            return cb(null, true)
          }).catch(function (err){
            return cb(err, null)
          })
        }).catch(function (err){
          return cb(err, null)
        })
      }]
    }

    async.auto(plays, function (err, results){
      if(err) { return res.status(406).json(err).end(); }
      console.log("RODOU Content");
      results.saveContent.getExtras().then(function (){
        console.log("RODOU Extras");
        //return results.saveContent.writeJSON().then(function (){
            console.log("RODOU JSON");
            return results.saveContent.writeSiteContent()
        //})
      }).catch(function (){}).done(function (){
        console.log("RODOU Error");
        res.status(201).json(results.saveContent).end();
      })
    })


  }
  this.create = function(req, res, next){
    var obj = allowedFields(req.body);
    obj.SiteId = req.site.id;

    if(parseInt(req.body.thumbnailID)){
      var thumbnailID = parseInt(req.body.thumbnailID);
      var thumbnailCredit = (!req.body.thumbnailCredit)?"Divulgação":req.body.thumbnailCredit;
      var thumbnailTitle = (!req.body.thumbnailTitle)?"":req.body.thumbnailTitle;
      var thumbnailText = (!req.body.thumbnailText)?"":req.body.thumbnailText;
      var thumbnailSize = (!req.body.thumbnailSize)?"":req.body.thumbnailSize;
      var thumbnail = Files.find(thumbnailID);
    }
    if(req.body.tags_ids){
      var tags = Tags.findAll({where: {id: {$in: req.body.tags_ids}}});
    }
    if(req.body.editorials_ids){
      var editorials = Editorials.findAll({where: {id: {$in: req.body.editorials_ids}}})
    }

    if(req.body.extras_ids && req.body.extras_ids.length > 1){
      var contentsExtras = Contents.findAll({where: {id: {$in: req.body.extras_ids}}})
    }
    var plays = {
      createContent : function(cb){
        Contents.create(obj).then(function (content){
          return cb(null, content);
        }).catch(function (err){
          return cb(err, null);
        })
      }
    }
    if(thumbnail) {
      plays.thumbnails = ['createContent', function(cb, result){
        thumbnail.then(function (file){
          FilesContent.create({
            type: "THUMBNAIL",
            FileId: file.id,
            ContentId: result.createContent.id,
            credits: thumbnailCredit,
            title: thumbnailTitle,
            fileSize: thumbnailSize,
            description: thumbnailText
          }).then(function (thumbnail){
            return cb(null, thumbnail);
          }).catch(function (err){

            return cb(err, null);
          })
        })
      }]
    }
    if(tags) {
      plays.tags = ['createContent', function(cb, result){
        tags.then(function (tagsFinded){
          result.createContent.setTags(tagsFinded).then(function (){

            __.forEach(tagsFinded, function (tag, index){
                //tag.writeContentJSON(result.createContent.SiteId);
            })


            return cb(null, true)
          }).catch(function (err){
            return cb(err, null)
          })
        }).catch(function (err){
          return cb(err, null)
        })
      }]
    }
    if(editorials) {
      plays.editorials = ['createContent', function(cb, result){
        editorials.then(function (editiorialsFinded){

          result.createContent.setEditorials(editiorialsFinded).then(function (){
            __.forEach(editiorialsFinded, function (editorial, index){
              //editorial.contentEditorialsPath();
              //editorial.writeContentTypeJson(result.createContent.type);
            })

            return cb(null, true)
          }).catch(function (err){

            return cb(err, null)
          })
        }).catch(function (err){

          return cb(err, null)
        })
      }]
    }

    if(contentsExtras){

      plays.extras = ['saveContent', function (cb, result){
        contentsExtras.then(function (contents){

          result.saveContent.setChildren([]).then(function (){
            async.eachLimit(contents, 1, function (item, cb2){
              result.saveContent.addChildren(item).then(function (){
                return cb2(null, item);
              })
            }, function (err){
              return cb(null, contents)
            })

          })
        }).catch(function (err){
          return cb(err, null);
        })
      }]
    }

    async.auto(plays, function (err, results){
      if(err) { return res.status(406).json(err).end(); }
      /*
      var cont = results.createContent.writeJSON();
      if(cont){
        cont.then(function (){
          results.createContent.writeSiteContent()
        })
      } else {
        results.createContent.writeSiteContent()
      }

      res.status(201).json(results.createContent).end();
      */


      //results.createContent.writeJSON().then(function (){
      //    return results.createContent.writeSiteContent()
      //}).catch(function (){}).done(function (err){
          res.status(201).json(results.createContent).end();
      //})


    })
    /*
    var nContent;
    Contents.create(obj).then(function (content){
      nContent = content;

      if(thumbnail) {
        return thumbnail.then(function (file){
          return FilesContent.create({
            type: "THUMBNAIL",
            FileId: file.id,
            ContentId: content.id
          })
        })
      }
      //return res.status(201).json(content);
    }).catch(function (err){
      return next({status: 500, message: "Unable to create Content", debug: err});
    }).done(function (rest){
      if(nContent){
        return res.status(201).json(nContent).end();
      }
    })*/

    //
  }

  this.search = function (req, res){
    var page = (req.query.page)?parseInt(req.query.page):0;
    var perPage = (req.query.perPage)?parseInt(req.query.perPage):10;



    models.Content.findAndCountAll({
      where: {
        status: "published",
        //publishedAt: {$lte: new Date()},

        $or: [
        {
          title: {
            $like: req.query.q+'%'
          }
        },
        {
          summary: {
            $like: '%'+req.query.q+'%'
          }
        }

      ]},
     include: [{model: models.FilesContent, where: {type: "THUMBNAIL"}, include: [{model: models.File}]}],
     offset: (perPage * page), limit: perPage, order: [['publishedAt', 'DESC']]
    }).then(function (result){
      return res.status(200).json(result).end();
    }).catch(function (err){
        return res.status(500).end();
    })


  }

  this.votes = function (req, res, next){
    if(req.body.votes){
      var index = parseInt(req.body.votes);
    }

    if(index > 0){
      req.content["votos"+index] += 1;

      return req.content.save().then(function (content){
        //return req.content.writeJSON();
      //}).then(function (){
        return res.status(202).json({status: "OK"}).end();
      })
    }

    return res.status(406).json({status: "NOT_OK"}).end();
  }
  this.remove = function (req, res, next){
    Contents.find({
      where: {
        id: req.params.id,
        SiteId: req.site.id
    }}).then(function (content){
      return content.destroy()
    }).then(function (content){
      return res.status(201).end();
    }).catch(function (err){
      return next({err: 500, message: "Unable to delete content"})
    })
  }
  return this;
}
