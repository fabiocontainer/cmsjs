var __ = require("underscore");
var models  = require('../../../models');
var Editorials = models.Editorial
var moment = require("moment");
var fs = require("fs");
var path = require("path");

function allowedFields(params){
  var obj = {};
  var fields = ["name", "title", "keywords", "description", "template", "url"];
  __.forEach(params, function (value, index){
    if(fields.indexOf(index) >= 0){
      obj[index] = value;
    }
  })
  return obj;
}

module.exports = function (){
  this.index = function (req, res){
    Editorials.findAll({where: {ParentId: req.editorial.id}}).then(function (results){
      return res.status(200).json(results).end;
    }).catch(function (err){
      return res.status(503).end;
    })
  }
  this.remove = function (req, res, next){
    Editorials.find(req.params.id).then(function (item){
      item.destroy().then(function (){
        return res.status(201).end();
      }).catch(function(err){
          return res.status(503).end();
      })
    }).catch(function(err){
      return next("Editorial not Found");
    })
  }
  // Sites
  this.sites = function (req, res, next){
    Editorials.findAll({where: {ParentId: null, routeLevel: 1}}).then(function (sites){
      return res.status(200).json(sites).end();
    }).catch(function (){
      return next({status: 500, message: "Cannot load Sites"});
    })
  }
  
  this.homes = function (req, res, next){
    Editorials.findAll({where: {SiteId: req.site.id, template: {$ne: null}, mobileTemplate: {$ne: null}, tabletTemplate: {$ne: null}}}).then(function (editorials){
      return res.status(200).json(editorials).end();
    }).catch(function (){
      return res.status(500).end();
    })
  }
  this.create = function (req, res){
    var obj = allowedFields(req.body);
    obj.ParentId = req.editorial.id;
    obj.routeLevel = (req.editorial.routeLevel + 1);
    
    Editorials.create(obj).then(function (editorial){
      return res.status(201).json(editorial).end();
    }).catch(function (err){
      return res.status(500).json({error: err}).end();
    })
  }
  this.featureds = function (req, res, next){  
    var cDate = moment().format('YYYY-MM-DD HH:mm:ss');
    Editorials.find({
      where: {
        id: req.params.id
      }, 
      include: [{
        model: models.Feature, 
        where: {
          startDate: {
            $lte: cDate
          },
          endDate: {
            $gte: cDate
          }
        },
        include: [models.File]
      }]
    }).then(function (item){
      return res.status(200).json(item).end();
      
    }).catch(function(err){
      return next({status: 500, message: "Editorial not Found", debug: err});
    })
  }
  this.show = function (req, res, next){  
    //var cDate = moment().format('YYYY-MM-DD HH:mm:ss');
    Editorials.find({
      where: {
        id: req.params.id
      }}).then(function (item){
      return res.status(200).json(item).end();
      
    }).catch(function(err){
      return next({status: 500, message: "Editorial not Found", debug: err});
    })
  }
  this.update = function (req, res, next){
    var obj = allowedFields(req.body);
    obj.ParentId = req.editorial.id;
    obj.routeLevel = (req.editorial.routeLevel + 1);
    
    Editorials.find(req.params.id).then(function (item){
      __.forEach(obj, function (value, index){
        item[index] = value;
      })
      item.save().then(function (){
        return res.status(201).end();
      }).catch(function(err){
          return res.status(503).end();
      })
    }).catch(function(err){
      return next("Editorial not Found");
    })
  }
  this.css = function (req, res, next){
    var format = (!req.query.media)?"DESKTOP":(req.query.media.toUpperCase());
    var template = "";
    switch(format){
      case "TABLET":
        template  = req.editorial.tabletTemplate;
      break;
      case "MOBILE":
        template  = req.editorial.mobileTemplate;
      break;
      case "DESKTOP":
        template = req.editorial.template;
      break;
    }
    
    var pathTemplate = path.join(req.app.locals.dirPath, 'templates', format, template, "style.css");
    
    res.sendFile(pathTemplate, function (err){
      if(err){ return res.status(err.status).end() }
    })
  }
  this.template = function (req, res, next){
    var format = (!req.query.media)?"DESKTOP":(req.query.media.toUpperCase());
    var template = "";
    switch(format){
      case "TABLET":
        template  = req.editorial.tabletTemplate;
      break;
      case "MOBILE":
        template  = req.editorial.mobileTemplate;
      break;
      case "DESKTOP":
        template = req.editorial.template;
      break;
    }
    
    
    var pathTemplate = path.join(req.app.locals.dirPath, 'templates', format, template, "index.js");
    
    res.sendFile(pathTemplate, function (err){
      if(err){ return res.status(err.status).end() }
    })
  }
  return this;
}
